import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { AccountService } from './services/account.service';
import { LoaderService } from './services/loader.service';

@Component({
  selector: 'tt-root',
  template: `
    <tt-menu *ngIf="isAuthorized" class="layout-row layout-align-space-between-center"></tt-menu>
    <tt-loader *ngIf="loaderIsOn"></tt-loader>
    <router-outlet></router-outlet>
  `
})
export class AppComponent implements OnInit {
  isAuthorized: boolean;
  loaderIsOn: boolean = true;

  constructor(private router: Router, private accountService: AccountService, private loaderService: LoaderService) {
    this.isAuthorized = localStorage['authKey'] ? true : false;
  }

  ngOnInit() {

      this.loaderService.switchLoader.subscribe(turnLoader => this.loaderIsOn = turnLoader);

      this.accountService.onAuthorisationChanges.subscribe(
          isAuthorized => {
            this.isAuthorized = isAuthorized;
            if (this.isAuthorized === false) {
              this.router.navigate(['/']);
            }
          }
      );
  }

}


    // <tt-account></tt-account>