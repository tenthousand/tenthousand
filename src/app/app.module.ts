import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ChartModule } from 'angular2-highcharts';

import { routing } from './app.rounting';

import { AppComponent } from './app.component';
import { LoaderComponent } from './components/shared/loader.component';
import { AccountComponent } from './components/account/account.component';
import { AccountActionComponent } from './components/account/account-action.component';
import { FormGroupComponent } from './components/shared/form-group.component';
import { MainComponent } from './components/main/main.component';
import { HighchartsComponent } from './components/shared/highcharts.component';
import { MenuComponent } from './components/main/menu.component';
import { TargetsComponent } from './components/targets/targets.component';
import { RoutineComponent } from './components/routine/routine.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { SitetourComponent } from './components/shared/sitetour.component';
import { TargetsSettingsComponent } from './components/targets/targets-settings.component';
import { TargetsSettingsGlobalComponent } from './components/targets/targets-settings-global.component';
import { TargetsSettingsLongComponent } from './components/targets/targets-settings-long.component';
import { TargetsSettingsShortComponent } from './components/targets/targets-settings-short.component';
import { TargetsOverviewComponent } from './components/targets/targets-overview.component';
import { RoutineSettingsComponent } from './components/routine/routine-settings.component';
import { DoingSettingComponent } from './components/routine/doing-setting.component';
import { HabitSettingsComponent } from './components/routine/habit-settings.component';
import { HabitSettingsSectionComponent } from './components/routine/habit-settings-section.component';
import { PeriodComponent } from './components/period/period.component';
import { PeriodPlanComponent } from './components/period/period-plan.component';
import { PeriodReportComponent } from './components/period/period-report.component';
import { PeriodStatComponent } from './components/period/period-stat.component';
import { PlanTaskComponent } from './components/period/plan-task.component';
import { ReportTaskComponent } from './components/period/report-task.component';

import { HttpService } from './services/http.service';
import { AccountService } from './services/account.service';
import { ConvertService } from './services/convert.service';
import { LoaderService } from './services/loader.service';

import { TargetDirective } from './directives/target.directive';
import { DropdownDirective } from './directives/dropdown.directive';
import { TargetsOverviewTargetComponent } from './components/targets/targets-overview-target.component';
import { PopupComponent } from './components/main/popup.component';
import { PopupTargeteditComponent } from './components/shared/popups/popup-targetedit.component';
import { CalendarDayComponent } from './components/calendar/calendar-day.component';

@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent,
    AccountComponent,
    AccountActionComponent,
    FormGroupComponent,
    MainComponent,
    HighchartsComponent,
    MenuComponent,
    TargetsComponent,
    RoutineComponent,
    CalendarComponent,
    TargetDirective,
    DropdownDirective,
    SitetourComponent,
    TargetsSettingsComponent,
    TargetsSettingsGlobalComponent,
    TargetsSettingsLongComponent,
    TargetsSettingsShortComponent,
    TargetsOverviewComponent,
    RoutineSettingsComponent,
    DoingSettingComponent,
    HabitSettingsComponent,
    HabitSettingsSectionComponent,
    PeriodComponent,
    PeriodPlanComponent,
    PeriodReportComponent,
    PeriodStatComponent,
    PlanTaskComponent,
    ReportTaskComponent,
    TargetsOverviewTargetComponent,
    PopupComponent,
    PopupTargeteditComponent,
    CalendarDayComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ChartModule,
    routing
  ],
  providers: [HttpService, AccountService, ConvertService, LoaderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
