import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class LoaderService {

  public switchLoader = new EventEmitter<boolean>();

  constructor() { }

  onSwitchLoader(state: boolean) {
    this.switchLoader.emit(state);
  }

}
