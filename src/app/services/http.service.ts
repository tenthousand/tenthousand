import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { User } from '../models/user';
import { Target } from '../models/target';
import { Doing } from '../models/doing';
import { Habit } from '../models/habit';
import { Task, SendTask } from '../models/task';
import { Period } from '../models/calendar';

const url = 'http://api.10000.ru/index.php/site/facade';

@Injectable()
export class HttpService {

  constructor(private http: Http) {}

  private toTimeString(d) {
    return d.getFullYear() + pad(d.getMonth() + 1) + pad(d.getDate());
    function pad(number) { if (number < 10) return '0' + number; else return number;}
  };

  private setHeaders(){
    const headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=UTF-8');
    headers.append('Authorization', localStorage['authKey']);
    return headers;
  }
  private mapTaskToSave(task: Task, period: Period) {
    console.log('task: ',task);
    var sendTask: SendTask = {
      isDone: task.isDone ? task.isDone : false,
      title: task.title,
      date: period.endDate instanceof Date ? parseInt(this.toTimeString(period.endDate)) : period.endDate, 
      dateType: period.dateType, 
      parent_ref: null,
      spentTime: task.spentTime ? task.spentTime : 0,
      target_ref: task.target_ref, 
      comment: task.comment ? task.comment :'', 
      isUrgent: task.isUrgent, 
      isImportant: task.isImportant, 
      isMain: task.isMain,
    }
    if (task.id) sendTask.id = task.id;
    return sendTask;
  }

  //Tasks
  saveTask(task: Task, period: Period) {
    let sendTask = this.mapTaskToSave(task, period);
    console.log(sendTask);
    let data: any = {method: sendTask.id ? 'task/edit' : 'task/create', body: sendTask};
    const body = JSON.stringify(data);
    let headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers })
      .map((data: Response) => data.json())
      .catch(this.handleError);
  }
  getTasksList(period) {
    let data: any = { method: 'period/findtasks', 
      body:{
        dateType: period.dateType, 
        dateFrom: period.startDate instanceof Date ? this.toTimeString(period.startDate) /*(parseInt(this.toTimeString(period.startDate)) - 1).toString()*/ : period.startDate, 
        dateTo: period.endDate instanceof Date ? this.toTimeString(period.endDate) /*(parseInt(this.toTimeString(period.endDate)) + 2).toString()*/ : period.endDate 
      }
    };
    // console.log(data);
    // console.log((parseInt(this.toTimeString(period.startDate)) - 1).toString());
    // console.log((parseInt(this.toTimeString(period.endDate)) + 2).toString());
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers }).map((data: Response) => data.json()).catch(this.handleError);
  }
  setDayTasks(task: Task, period: Period) {
      let sendTask = this.mapTaskToSave(task, period);
      let data: any = { method: 'task/edit', body:sendTask};
      const body = JSON.stringify(data);
      const headers = this.setHeaders();
      return this.http.post(url, body, { headers: headers }).map((data: Response) => data.json()).catch(this.handleError);
  }

  // Targets
  saveTarget(target: Target) {
    let data: any = { body:{ target_name: target.target_name }};
    if (target.id) {
      data.method = 'target/edit'; 
      data.body.id = target.id;
      data.body.deadline = target.deadline ? target.deadline : new Date();
      data.body.description = target.description ? target.description : '';
    } else data.method = 'target/create';
    // console.log(target);
    if (target.parent_ref) data.body.parent_ref = target.parent_ref;
    const body = JSON.stringify(data);
    let headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers })
      .map((data: Response) => data.json())
      .catch(this.handleError);
  }
  deleteTarget(id: string) {
    if (id != undefined) {
      let data: any = { body:{ id }};
      data.method = 'target/delete';;
      const body = JSON.stringify(data);
      let headers = this.setHeaders();
      return this.http.post(url, body, { headers: headers })
        .map((data: Response) => data.json())
        .catch(this.handleError);
    }
  }
  getTargetChildren(parent_ref?: string) {
    let data: any = { method: 'target/list', body: { parent_ref }};
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers })
      .map((data: Response) => data.json())
      .catch(this.handleError);
  }
  getTargetInfo(id: string) {
    if (!id) {
      console.log('no ID for target/get')
      return;
    }
  
    let data: any = { method: 'target/get', body: { id }};
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers })
      .map((data: Response) => data.json())
      .catch(this.handleError);
  }

  // Habits
  habitSave(habit: Habit) {
    let data: any = { body:{ title: habit.title, type: habit.type, duration: habit.duration }};
    if (habit.id) { data.method = 'habit/edit'; data.body.id = habit.id; }
    else data.method = 'habit/create';
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers }).map((data: Response) => data.json()).catch(this.handleError);
  }
  habitDelete(id: string) {
    let data: any = { method: 'habit/delete',body:{ id }};
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers }).map((data: Response) => data.json()).catch(this.handleError);
  }
  getHabitsList() {
    let data: any = { method: 'habit/list', body:{}};
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers }).map((data: Response) => data.json()).catch(this.handleError);
  }
  setDayHabits(habit: Habit, day: Period) {
    let data: any = { method: 'habit/setperiod', body:{id: habit.id, date: this.toTimeString(day.endDate)}};
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers }).map((data: Response) => data.json()).catch(this.handleError);
  }

  // Doings
  doingSave(doing: Doing) {
    let data: any = { body:{ title: doing.title, idealTime: doing.idealTime, period: doing.period }};
    if (doing.id) { data.method = 'doing/edit'; data.body.id = doing.id; }
    else data.method = 'doing/create';
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers }).map((data: Response) => data.json()).catch(this.handleError);
  }
  doingSеtPeriod(doing: Doing, date: Date, elapsed: number) {
    let data: any = {method: 'doing/setperiod', body:{ id: doing.id, date: this.toTimeString(date), elapsed }};
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers }).map((data: Response) => data.json()).catch(this.handleError);
  }
  doingDelete(id: string) {
    let data: any = { method: 'doing/delete',body:{ id }};
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers }).map((data: Response) => data.json()).catch(this.handleError);
  }
  getDoingsList() {
    let data: any = { method: 'doing/list', body:{}};
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers }).map((data: Response) => data.json()).catch(this.handleError);
  }
  setDayDoings(doing: Doing, day: Period) {
    let data: any = { method: 'doing/setperiod', body:{id: doing.id, date: this.toTimeString(day.endDate), elapsed: doing.spentTime ? doing.spentTime : doing.idealTime}};
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers }).map((data: Response) => data.json()).catch(this.handleError);
  }

  getPeriodHabits(period) {
    let data: any = { method: 'period/findhabits', 
      body:{
        dateFrom: period.startDate instanceof Date ? this.toTimeString(period.startDate) /*(parseInt(this.toTimeString(period.startDate)) - 1).toString()*/ : period.startDate, 
        dateTo: period.endDate instanceof Date ? this.toTimeString(period.endDate) /*(parseInt(this.toTimeString(period.endDate)) + 2).toString()*/ : period.endDate 
      }
    };
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers }).map((data: Response) => data.json()).catch(this.handleError);
  }

  getPeriodDoings(period) {
    let data: any = { method: 'period/finddoings', 
      body:{
        dateFrom: period.startDate instanceof Date ? this.toTimeString(period.startDate) /*(parseInt(this.toTimeString(period.startDate)) - 1).toString()*/ : period.startDate, 
        dateTo: period.endDate instanceof Date ? this.toTimeString(period.endDate) /*(parseInt(this.toTimeString(period.endDate)) + 2).toString()*/ : period.endDate 
      }
    };
    const body = JSON.stringify(data);
    const headers = this.setHeaders();
    return this.http.post(url, body, { headers: headers }).map((data: Response) => data.json()).catch(this.handleError);
  }


  logIn(user: User) {
    const body = JSON.stringify({
      method: 'user/login', 
      body: user
    })
    const url = 'http://api.10000.ru/index.php/site/facade';
    const headers = new Headers();
    headers.append('Content-Type', 'application/json; charset=UTF-8');
    return this.http.post(url, body, {
      headers: headers
    })
      .map((data: Response) => data.json())
      .catch(this.handleError);
  }

  private handleError (error: any) {
    console.log(error.json());
    return Observable.throw(error.json());
  }


}