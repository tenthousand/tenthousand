import { Injectable } from '@angular/core';

const timeRegexp = /\d{1,2}[h||ч||m||м](\d{1,2}[h||ч||m||м])?/g;

@Injectable()
export class ConvertService {

  constructor() { }

  setTime(timeString: string)  {
    let result;
    if (timeString.match(timeRegexp)) {
      result = timeString.match(timeRegexp)[0];
      let time;
      if (result.length > 3) {
        let hoursAndMinutes = result.match(/\d+/g);
        time = parseInt(hoursAndMinutes[0])*60 + parseInt(hoursAndMinutes[1]);
      }
      else if (result.length <= 3) {
        time = result.match(/h|ч/) ? parseInt(result) * 60 : parseInt(result);
      }
      return time;
    } else return 0;
  }

  setTimeString(time: number){
    return time > 59 ? Math.floor(time / 60) + 'ч' + (time % 60) + 'м' : time + 'м';
  }

  setPeriod(params) {
    let period,
        dateType = params['dateType'],
        year = params['year'],
        month = params['month'] ? parseInt(params['month']) : undefined,
        firstDay = params['startDate'] ? parseInt(params['startDate']) : undefined,
        lastDay = params['endDate'] ? parseInt(params['endDate']) : undefined,
        endMonth = params['endMonth'] ? parseInt(params['endMonth']) : undefined,
        endYear = params['endYear'] ? parseInt(params['endYear']) : undefined;
    if (month === undefined) period = { dateType, year, startDate: new Date(year, 0), endDate: new Date(parseInt(year) + 1, 0), type: 4} // Год
    else if (firstDay === undefined) period = { dateType, year, month, startDate: new Date(year, month), endDate: new Date(year, month + 1), type: 3 } // Месяц
    else if (lastDay === undefined) period = { dateType, year, month, firstDay, startDate: new Date(year, month, firstDay), endDate: new Date(year, month, firstDay), type: 1 } // День
    else period = { dateType, year, month, firstDay, lastDay, startDate: new Date(year, month, firstDay), endDate: new Date(endYear, endMonth, lastDay), type: 2 } // Неделя
    return period;
  }

}
