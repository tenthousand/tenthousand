import { Injectable, EventEmitter } from '@angular/core';

import { Target } from '../models/target';
import { HttpService } from "./http.service";

@Injectable()
export class AccountService {
  
  private mainTarget: Target;

  public onAuthorisationChanges = new EventEmitter<boolean>();

  constructor(private httpService: HttpService) { }

  onAuthorized(isAuthorized: boolean) {
    this.onAuthorisationChanges.emit(isAuthorized);
  }

  onLogOut() {
    localStorage.removeItem('authKey');
    this.onAuthorisationChanges.emit(false);
  }

  getMainTarget(num) {
    return new Promise((resolve) => {
      this.getTaskChildren(num, returnTarget);
      function returnTarget (target) {
        resolve(target);
      }
    })
  }

  getTaskChildren(target_type_ref: number, callback: Function, target_ref?: string): any {
    return new Promise((resolve) => {
      this.httpService.getTargetChildren(target_ref).subscribe(
        data => {
          if (target_type_ref === 0) {
            if (data && data.length) {
              this.mainTarget = data[0];
              this.getTaskChildren(1, callback, this.mainTarget.id);
            }
          } else if (target_type_ref === 1) {
            if (data && data.length) {
              this.mainTarget.children = data;
              for(let global of this.mainTarget.children) this.getTaskChildren(2, callback, global.id);
            }
          } else if (target_type_ref === 2) {
            if (data && data.length) 
              for (let global of this.mainTarget.children) 
                if (global.id === target_ref) {
                  global.children = data;
                  for(let long of global.children) this.getTaskChildren(3, callback, long.id);
                  break;
                } 
          } else if (target_type_ref === 3) {
            if (data && data.length) 
              for (var g in this.mainTarget.children)
                for (var l in this.mainTarget.children[g].children) {
                  if (this.mainTarget.children[g].children[l].id === target_ref) {
                    this.mainTarget.children[g].children[l].children = data;
                    if (parseInt(g) == (this.mainTarget.children.length - 1) && parseInt(l) == (this.mainTarget.children[g].children.length - 1)) {
                      callback(this.mainTarget);
                    }
                    break;
                  }
                }
          }
        }
      );
    });
  }


}

