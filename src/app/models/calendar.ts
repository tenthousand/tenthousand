import { Task } from './task';

export class Year {
  constructor (
    public tasks?: Task[],
    public alreadyReported?: boolean
  ) {}
}

export class Month {
  constructor (
    public year: number,
    public monthNumber: number,
    public daysInMonth: number,
    public firstDay: number,
    public days: Array<Day>,
    public tasks?: Task[],
    public alreadyReported?: boolean
  ) {}
}

export class Week {
  constructor (
    public firstDay: {
      date:number,
      month:number,
      year:number
    },
    public lastDay: {
      date:number,
      month:number,
      year:number
    },
    public tasks?: Task[],
    public alreadyReported?: boolean
  ) {}
}

export class Period {
  constructor (
    public startDate: Date,
    public endDate: Date,
    public year: number,
    public dateType: number,
    public month?: number,
    public firstDay?: number,
    public lastDay?: number,
  ) {}
}

export class Day {
  constructor (
    public date: number,
    public tasks?: Task[],
    public alreadyReported?: boolean
  ) {}
}