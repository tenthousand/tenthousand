export class FormGroup {
    constructor (public id: string, public value: string, public type: string, public name: string, public error: string) {}
}
