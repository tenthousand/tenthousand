export class Doing {
    constructor (
        public title: string, 
        public idealTime: number, 
        public period: number,
        public spentTime?: number, 
        public id?: string
    ) {}
}
