export class Habit {
    constructor (
        public title: string, 
        public type: number, 
        public done?: boolean,
        public duration?: number,
        public id?: string
    ) {}
}
