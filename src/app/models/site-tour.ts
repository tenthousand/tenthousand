export class SiteTour {
    constructor (
        public title: String,
        public description: String,
        public show: Boolean
    ) {}
}
