export class Target {
    constructor (
        public target_name: string, 
        public target_type_ref: number,
        public description?: string,
        public deadline?: Date,
        public children?: Target[],
        public id?: string,
        public parent_ref?: string,
        public showChildren?: boolean
    ) {}
}

export class DateObject {
    constructor (
        public year: number,
        public month: number,
        public date: number,
    ) {}
}
