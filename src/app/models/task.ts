export class Task {
    constructor (
        public title: string,
        public isUrgent: boolean,
        public isImportant: boolean,
        public isMain: boolean,
        public spentTime?: number,
        public isDone?: boolean,
        public dateType?: number,
        public target_ref?: string,
        public target_ref_title?: string,
        public comment?: string,
        public parent_ref?: {
            id: string,
            title: string
        },
        public id?: string,
        public date?: {
            year: number,
            month: number,
            day: number
        }
    ) {}
}

export class SendTask {
    constructor (
        public title: string,
        public isUrgent: boolean,
        public isImportant: boolean,
        public isMain: boolean,
        public spentTime?: number,
        public isDone?: boolean,
        public dateType?: number,
        public target_ref?: string,
        public target_ref_title?: string,
        public comment?: string,
        public parent_ref?: {
            id: string,
            title: string
        },
        public id?: string,
        public date?: number
    ) {}
}
