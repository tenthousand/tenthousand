export class ColumnChart {
    constructor (
        public doingsNames: string[], 
        public idealDoingsTime: number[], 
        public realDoingsTime: number[],
    ) {}
}
