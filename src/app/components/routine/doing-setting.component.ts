import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Doing } from '../../models/doing';

import { ConvertService } from '../../services/convert.service';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'tt-doing-setting',
  template: `
    <span (click)="onDeleteDoingClick()" class="flex-5 pt-4 ta-c tt-doing-setting-delete" title="Удалить это дело">
      <i class="fa fa-times"></i>
    </span>
    <span [ngClass]="{'done':statusDoing}" class="flex-5 pt-4 ta-c tt-doing-setting-done" title="{{statusDoing ? 'Настройка завершена' : 'Настройка дела не закончена' }}">
      <i class="fa fa-check"></i>
    </span>
    <span class="ph-4 flex-60 tt-doing-setting-title" title="Название дела">
      <input class="w100 input-bottom" type="text" [(ngModel)]="doing.title" (change)="onDoingInputChanged()">
    </span>
    <span class="ph-4 flex-25 tt-doing-setting-timeset relative" title="Укажите время в формате 9ч20м">
      <input class="w100 input-bottom" type="text" value="{{timeString}}" (change)="setTime($event)">
    </span>
    <span [ngClass]="{'tt-doing-setting-period-week':doing.period}" class="flex-5 pt-4 ta-c tt-doing-setting-period" (click)="onDoingPeriodChanged()" title="{{doing.period ? 'Установить как ежедневное дело' : 'Установить как еженедельное дело'}}">
      <i class="fa fa-calendar"></i>
    </span>
  `
})
export class DoingSettingComponent implements OnInit {

  private timeString: string = '';
  private statusDoing: boolean = false;
  
  @Output() deleteDoing = new EventEmitter<boolean>();
  @Output() changeDoingTime = new EventEmitter<boolean>();
  @Input() doing: Doing;

  constructor(private convertService: ConvertService, private httpService: HttpService) { }

  ngOnInit () {
    if (this.doing.title.length > 0 && this.doing.idealTime > 0) this.statusDoing = true;
    if (this.doing.idealTime) this.timeString += this.convertService.setTimeString(this.doing.idealTime);
  }

  setTime(e) {
    this.doing.idealTime = this.convertService.setTime(e.target.value);
    this.onDoingInputChanged();
    this.changeDoingTime.emit();
  }

  onDoingPeriodChanged() {
    this.doing.period = this.doing.period == 1 ? 0 : 1;
    this.onDoingInputChanged();
    this.changeDoingTime.emit();
  }
  onDoingInputChanged() {
    if (this.doing.title.length > 0 && this.doing.idealTime > 0) {
      this.statusDoing = true;
      this.httpService.doingSave(this.doing).subscribe( id => this.doing.id = id );
    } else this.statusDoing = false;
  }

  onDeleteDoingClick() {
    this.deleteDoing.emit();
    if (this.doing.id) this.httpService.doingDelete(this.doing.id).subscribe( response => console.log(response) );
  }

}
