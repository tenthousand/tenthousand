import { Component, OnInit, Input } from '@angular/core';

import { Habit } from '../../models/habit';

import { HttpService } from "../../services/http.service";

@Component({
  selector: 'tt-habit-settings-section',
  template: `
    <section *ngIf="action === 'setting'">
      <div class="sitetour__header w100">
        <h4 class="tt-habit-settings__section-title">{{title}}</h4>
      </div>
      <ul class="tt-habit-settings__section-list" >
        <li [ngClass]="{'disp-n': habit.type != type}" class="input-row tt-habit-settings__section-list-item layout-row layout-align-center-center" *ngFor="let habit of habits; let i = index">
          <div *ngIf="habit.type == type">
            <span (click)="onDeleteHabitClick(i, habit)" class="tt-habit-settings__section-button flex-10"><i class="tt-habit-settings__section-habit-delete fa fa-times"></i></span>
            <input (change)="onSaveHabit(habit)" class="tt-habit-settings__section-input input-bottom flex-90" [(ngModel)]="habit.title" type="text">
          </div>
        </li>
      </ul>
      <div class="layout-row layout-align-start-center">
        <span class="tt-habit-settings__section-button flex-10"><i class="tt-habit-settings__section-habit-add fa fa-plus" (click)="onAddHabitClick()"></i></span>
      </div>
    </section>
    <section *ngIf="action === 'report'">
      <h4 class="tt-habit-settings__section-title">{{title}}</h4>
      <div [ngClass]="{'disp-n': habit.type != type}" class="" *ngFor="let habit of habits; let i = index">
        <div *ngIf="habit.type == type">
          <button (click)="habit.done = !habit.done" [ngClass]="{'done':habit.done}" class="mv-4 ml-4 tt-period-report__habits-button tt-period-report__habits-button-type_{{habit.type}} btn flex-90">{{habit.title}}</button>
        </div>
      </div>
    </section>
  `
})
export class HabitSettingsSectionComponent implements OnInit {

  @Input() habits: Habit[];
  @Input() title: string;
  @Input() type: number;
  @Input() action: number;

  constructor(private httpService: HttpService) { }

  ngOnInit() {
  }

  onDeleteHabitClick(i, habit) {
    if (habit.id)
      this.httpService.habitDelete(habit.id).subscribe(
        data => {
          console.log(data);
          this.habits.splice(i,1);
        }
      )
    else
      this.habits.splice(i,1);
  }

  onAddHabitClick() {
    this.habits.push({title:'', type: this.type, duration: 0})
  }
  
  onSaveHabit(habit) {
    if(habit.title.length)
      this.httpService.habitSave(habit).subscribe(
        data => {
          console.log(data);
        }
      )
  }

}
