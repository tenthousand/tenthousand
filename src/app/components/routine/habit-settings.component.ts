import { Component, OnInit, Input } from '@angular/core';

import { Habit } from '../../models/habit';

@Component({
  selector: 'tt-habit-settings',
  template: `
    <div class="layout-row layout-wrap h100">
      <tt-habit-settings-section class="tt-habit-settings__section tt-habit-settings__section-needwant habit-left habit-top flex-50"
                                 [action]="action" [habits]="habits" [type]="0" [title]="'Надо и хочу'"></tt-habit-settings-section>
      <tt-habit-settings-section class="tt-habit-settings__section tt-habit-settings__section-needwant habit-top flex-50"
                                 [action]="action" [habits]="habits" [type]="1" [title]="'Надо и не хочу'"></tt-habit-settings-section>
      <tt-habit-settings-section class="tt-habit-settings__section tt-habit-settings__section-needwantt habit-left flex-50"
                                 [action]="action" [habits]="habits" [type]="2" [title]="'Нельзя и хочу'"></tt-habit-settings-section>
      <tt-habit-settings-section class="tt-habit-settings__section tt-habit-settings__section-needwant flex-50"
                                 [action]="action" [habits]="habits" [type]="3" [title]="'Нельзя и не хочу'"></tt-habit-settings-section>
    </div>
  `
})
export class HabitSettingsComponent implements OnInit {

  @Input() action: string;
  @Input() habits: Habit[];

  constructor() { }

  ngOnInit() {}

}
