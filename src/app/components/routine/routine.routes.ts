import { Routes } from "@angular/router";

import { RoutineSettingsComponent } from "./routine-settings.component";
// import { RoutineOverviewComponent } from "./routine-overview.component";

export const ROUTINE_ROUTES: Routes = [
    { path: 'settings', component: RoutineSettingsComponent }
    // { path: 'overview', component: RoutineOverviewComponent }
];
