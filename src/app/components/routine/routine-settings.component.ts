import { Component, OnInit } from '@angular/core';

import { Doing } from '../../models/doing';
import { Habit } from '../../models/habit';

import { HttpService } from '../../services/http.service';


const defaultDoings: Doing[] = [
  {title: 'Гигиена', idealTime: 30, period: 0},
  {title: 'Время в дороге', idealTime: 60, period: 0},
  {title: 'Время на еду', idealTime: 30, period: 0},
  {title: 'Сон', idealTime: 420, period: 0},
  {title: '', idealTime: 0, period: 0},
];
const defaultHabits: Habit[] = [
  {title: 'Плотный завтрак', type: 0, duration: 0},
  {title: 'Зарядка', type: 1, duration: 0},
  {title: 'Компьютерные игры', type: 2, duration: 0},
  {title: 'Алкоголь', type: 3, duration: 0}
]

@Component({
  selector: 'tt-routine-settings',
  template: `
  <div class="layout-row">
    <section class="tt-routine-settings__section tt-routine-settings__doings relative flex-50">
      <div class="tt-routine-settings__doings-full-time">
        <h4 class="container-title">Общее время дел на неделю: <span title="{{routineFullTime !== 10080 ? 'Суммарное время недели должно быть 168 часов' : ''}}" [ngClass]="{'color-red':routineFullTime !== 10080}" class="tt-routine-settings__doings-full-time_span">{{routineFullTimeString}}</span></h4>
      </div>
      <div *ngFor="let doing of doings; let doingIndex = index">
        <tt-doing-setting class="layout-row input-row" (deleteDoing)="onDeleteDoing(doingIndex)" (changeDoingTime)="onChangeDoingTime()" [doing]="doing"></tt-doing-setting>
      </div>
      <div class="tt-routine-settings__doings-add layout-row">
        <span class="flex-5 ta-c pt-4">
          <i class="fa fa-plus" (click)="onAddDoing()"></i>
        </span>
      </div>
    </section>
    <section class="tt-routine-settings__section tt-routine-settings__doings flex-50">
      <tt-habit-settings class="h100" [habits]="habits" [action]="'setting'"></tt-habit-settings>
    </section>
  </div>
  `
})
export class RoutineSettingsComponent implements OnInit {

  public doings: Doing[];
  public habits: Habit[];
  private routineFullTimeString: string;
  private routineFullTime: number = 0;

  private filledDoings: number = 4;

  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.httpService.getDoingsList().subscribe(
      data => {
        console.log(data);
        if (data.doings && data.doings.length) this.doings = data.doings; else this.doings = defaultDoings;
        this.makeTimeString();
      }
    )
    this.httpService.getHabitsList().subscribe(
     data => {
       console.log(data);
        if (data.habitss && data.habitss.length) this.habits = data.habitss; else this.habits = defaultHabits;
     }
    )
  }

  onChangeDoingTime() {
    this.makeTimeString();
  }

  makeTimeString () {
    this.routineFullTime = 0;
    this.routineFullTimeString = '';
    this.doings.forEach( doing => this.routineFullTime += doing.period ? doing.idealTime : doing.idealTime * 7);
    this.routineFullTimeString += Math.floor(this.routineFullTime / 60) + 'ч' + (this.routineFullTime % 60) + 'м';
  }

  onAddDoing() {
    this.doings.push({title: '', idealTime: 0, period: 0});
  }

  onDeleteDoing (doingIndex) {
    console.log(doingIndex);
    this.doings.splice(doingIndex,1);
    this.makeTimeString();
  }

}
