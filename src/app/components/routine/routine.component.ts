import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tt-routine',
  template: `
  <router-outlet></router-outlet>
  `,
  styles: []
})
export class RoutineComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
