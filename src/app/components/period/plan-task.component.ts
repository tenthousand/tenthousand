import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Target } from '../../models/target';
import { Task } from '../../models/task';

@Component({
  selector: 'tt-plan-task',
  template: `
    <span (click)="onDeleteTaskClick()" class="flex-5 pt-4 ta-c input-row-delete" title="Удалить это дело">
      <i class="fa fa-times"></i>
    </span>
    <span [ngClass]="{'done':statusTask}" 
          class="input-row-done flex-5 pt-4 ta-c" 
          title="{{statusTask ? 'Настройка завершена' : 'Настройка задачи не закончена' }}">
      <i class="fa fa-check"></i>
    </span>
    <span class="relative ph-4 flex-30 input-row-title" title="Родительская цель" (click)="toggleTargetSelection(true)" (mouseleave)="toggleTargetSelection(false)">
      <span [ngClass]="{'text-placeholder':!task.target_ref}" class="nowrap disp-b tt-plan-task-target_ref">{{task.target_ref ? task.target_ref_title : 'Выберите цель'}}</span>
      <ul class="tt-plan-task-ul absolute" *ngIf="targetSelectionIsOpen">
        <li [ngClass]="{'tt-plan-task-choosing':global.showChildren}" class="relative tt-plan-task-ul-li" *ngFor="let global of targetModel.children; let g = index" (mouseenter)="global.showChildren = true" (mouseleave)="global.showChildren = false">
          <span class="nowrap disp-b tt-plan-task-ul-span">{{global.target_name}}</span>
          <ul class="absolute tt-plan-task-ul left100" *ngIf="global.showChildren">
            <li [ngClass]="{'tt-plan-task-choosing':long.showChildren}" class="relative tt-plan-task-ul-li" *ngFor="let long of global.children; let l = index" (mouseenter)="long.showChildren = true" (mouseleave)="long.showChildren = false">
              <span class="nowrap disp-b tt-plan-task-ul-span">{{long.target_name}}</span>
              <ul class="absolute tt-plan-task-ul left100" *ngIf="long.showChildren">
                <li [ngClass]="{'tt-plan-task-chosen': short.showChildren || (short.id == task.target_ref)}" class="tt-plan-task-ul-li" *ngFor="let short of long.children; let s = index" (mouseenter)="short.showChildren = true" (mouseleave)="short.showChildren = false">
                  <span class="nowrap disp-b tt-plan-task-ul-span last" (mousedown)="chooseParentTarget(short, long, global)" (click)="toggleTargetSelection(false)">{{short.target_name}}</span>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </span>
    <span class="ph-4 flex-45 input-row-title" title="Название задачи">
      <input placeholder="Название задачи..." class="w100 input-bottom" type="text" [(ngModel)]="task.title" (change)="onTaskInputChanged()">
    </span>
    <span [ngClass]="{'input-row-urgent':task.isUrgent}" class="flex-5 pt-4 ta-c input-row-sign" (click)="task.isUrgent = !task.isUrgent" title="{{task.isUrgent ? 'Срочное дело' : 'Несрочное дело'}}">
      <i class="fa fa-bolt"></i>
    </span>
    <span [ngClass]="{'input-row-important':task.isImportant}" class="flex-5 pt-4 ta-c input-row-sign" (click)="task.isImportant = !task.isImportant" title="{{task.isImportant ? 'Важное дело' : 'Неважное дело'}}">
      <i class="fa fa-flag"></i>
    </span>
    <span [ngClass]="{'input-row-main':task.isMain}" class="flex-5 pt-4 ta-c input-row-sign" (click)="task.isMain = !task.isMain" title="{{task.isMain ? 'Главное дело' : 'Неглавное дело'}}">
      <i class="fa fa-bullseye"></i>
    </span>
  `
})
export class PlanTaskComponent implements OnInit {

  @Input() task: Task;
  @Input() targetModel: Target;
  @Output() deleteTask = new EventEmitter<string>();
  @Output() taskStatusChanged = new EventEmitter<boolean>();

  targetSelectionIsOpen: boolean = false;
  statusTask: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  toggleTargetSelection(openOrClose) {
    this.targetSelectionIsOpen = openOrClose;
  }

  chooseParentTarget(target: Target, long, global) {
    global.showChildren = false;
    long.showChildren = false;
    this.task.target_ref = target.id;
    this.task.target_ref_title = target.target_name;
    this.toggleTargetSelection(false);
    this.checkTaskStatus();
  }

  onTaskInputChanged() {
    this.checkTaskStatus();
  }

  checkTaskStatus(){
    if (this.task.title.length > 0 && this.task.target_ref) {
      if(this.statusTask != true){
        this.statusTask = true;
        this.taskStatusChanged.emit(true);
      }
    }
    else {
      if(this.statusTask != false){
        this.statusTask = false;
        this.taskStatusChanged.emit(false);
      }
    }
  }

  onDeleteTaskClick() {
    this.deleteTask.emit();
  }

}
