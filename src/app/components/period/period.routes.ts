import { Routes } from "@angular/router";

import { PeriodPlanComponent } from "./period-plan.component";
import { PeriodReportComponent } from "./period-report.component";
import { PeriodStatComponent } from "./period-stat.component";

export const PERIOD_ROUTES: Routes = [
    { path: 'plan', component: PeriodPlanComponent },
    { path: 'report', component: PeriodReportComponent },
    { path: 'stat', component: PeriodStatComponent }
]
