import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Task } from '../../models/task';
import { Period } from '../../models/calendar';

import { ConvertService } from '../../services/convert.service';

@Component({
  selector: 'tt-report-task',
  template: `
    <span title="{{task.isDone ? 'Пометить как незавершенную': 'Пометить как завершенную'}}" class="pt-4 ta-c flex-5" [ngClass]="{'input-row-done':task.isDone}" (click)="task.isDone = !task.isDone"><i class="fa fa-check"></i></span>
    <span [ngClass]="{'flex-45':period.dateType != 1,'flex-35':period.dateType == 1}" title="{{task.title}}" class="pt-4 nowrap flex-35">{{task.title}}</span>
    <span *ngIf="period.dateType == 1" class="flex-10 mr-4"><input type="text" value="{{timeString}}" title="Потраченное время" class="w100 input-bottom tt-report-task__time" (change)="setTime($event)"></span>
    <span class="flex-35 mr-4"><input type="text" [(ngModel)]="task.comment" title="Комментарий по задаче" class="w100 input-bottom tt-report-task__comment"></span>
    <span title="Основное дело" class="pt-4 ta-c flex-5 input-row-main" *ngIf="task.isMain"><i class="fa fa-bullseye"></i></span>
    <span title="Важное дело" class="pt-4 ta-c flex-5 input-row-important" *ngIf="task.isImportant"><i class="fa fa-flag"></i></span>
    <span title="Срочное дело" class="pt-4 ta-c flex-5 input-row-urgent" *ngIf="task.isUrgent"><i class="fa fa-bolt"></i></span>
  `,
  styles: []
})
export class ReportTaskComponent implements OnInit {
// [(ngModel)]="task.spentTime"
  private timeString: string = '';
  @Input() task: Task;
  @Input() period: Period;

  @Output() taskReportChanged = new EventEmitter<Task>()

  constructor(private convertService: ConvertService) { }

  ngOnInit() {
    if (this.task.spentTime) this.timeString += this.convertService.setTimeString(this.task.spentTime);
  }

  onTaskReportChanged(){
    this.taskReportChanged.emit(this.task);
  }
  
  setTime(e) {
    this.task.spentTime = this.convertService.setTime(e.target.value);
    this.onTaskReportChanged();
  }

}
