import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute} from '@angular/router';

import { Target } from '../../models/target';
import { Task } from '../../models/task';
import { SiteTour } from '../../models/site-tour';
import { Period } from '../../models/calendar';

import { HttpService } from "../../services/http.service";
import { LoaderService } from '../../services/loader.service';
import { ConvertService } from '../../services/convert.service';
import { AccountService } from '../../services/account.service';

@Component({
  selector: 'tt-period-plan',
  template: `
    <div *ngFor="let task of tasks; let taskIndex = index">
      <tt-plan-task [task]="task" 
                    class="layout-row input-row" 
                    (deleteTask)="onDeleteTask(taskIndex)"
                    (taskStatusChanged)="onTaskStatusChanged($event)"
                    [targetModel]="mainTarget">
      </tt-plan-task>
    </div>
    <div class="tt-period-plan__tasks-add layout-row">
      <span class="flex-5 ta-c pt-4">
        <i class="fa fa-plus" (click)="onAddTask()"></i>
      </span>
    </div>
    <div class="layout-row layout-align-end-end">
      <button class="btn btn-primary" [disabled]="this.finishedTasks < this.tasks.length" (click)="onFinishPlan()">Завершить планирование</button>
    </div>
  `
})
export class PeriodPlanComponent implements OnInit, OnDestroy {
  
  public period: Period;
  private queryParams;
  private subscription: Subscription;
  private mainTarget: Target;
  private canFinishPlan: boolean = false;
  private finishedTasks: number = 0;
  private tasks: Task[] = [];

  constructor(private accountService: AccountService, private convertService: ConvertService, private httpService: HttpService, private loaderService: LoaderService, private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.queryParams.subscribe(params => {
      this.period = this.convertService.setPeriod(params);
      this.queryParams = params;
    });

    this.accountService.getMainTarget(0).then((mainTarget: Target) => this.mainTarget = mainTarget);
    this.getPlannedTasks();
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  // setPeriod(params) {
  //   this.period = this.convertService.setPeriod(params);
  // }

  getPlannedTasks() {
    this.httpService.getTasksList(this.period).subscribe(
      data => {
        if (data.tasks && data.tasks.length) {
          this.tasks = data.tasks;
          let tasksResponCount = 0;
          // for (let task of this.tasks) {
          //   this.httpService.getTargetInfo(task.target_ref).subscribe(
          //     (data) => {
          //       console.log(data);
          //       task.target_ref_title = data;
          //       tasksResponCount++;
          //       console.log(tasksResponCount + ' : ' + this.tasks.length);
          //       if (tasksResponCount == this.tasks.length)
          //         this.loaderService.onSwitchLoader(false);
          //     }
          //   );
          // }
          this.loaderService.onSwitchLoader(false);
        } else {
          this.tasks = [{ title: '', spentTime: 0, isUrgent: false, isImportant: false, isMain: false, isDone: false, dateType: this.period.dateType }];
          this.loaderService.onSwitchLoader(false);
        }
      }
    )
  }

  onTaskStatusChanged(finished: boolean){
    if (finished) this.finishedTasks += 1;
    else this.finishedTasks -= 1;
  }

  onFinishPlan() {
    let callbackCount = 0;
    this.tasks.forEach(
      task => this.httpService.saveTask(task, this.period).subscribe(data => {
        callbackCount++;
        if (callbackCount == this.tasks.length)
          this.router.navigate(['/period/report'], {queryParams: {year: this.period.year, month: this.period.month, startDate: this.period.startDate.getDate(), dateType: this.period.dateType}});
      })
    )
  }

  onDeleteTask(taskIndex: number) {
    this.tasks.splice(taskIndex,1);
  }

  onAddTask() {
    this.tasks.push({title:'',spentTime: 0,isUrgent: false, isImportant: false, isMain: false})
  }

  // getTaskChildren(target_type_ref: number, target_ref?: string): any {
  //   this.httpService.getTargetChildren(target_ref).subscribe(
  //     data => {
  //       if (target_type_ref === 0) {
  //         if (data && data.length) {
  //           this.mainTarget = data[0];
  //           this.getTaskChildren(1, this.mainTarget.id);
  //         }
  //       } else if (target_type_ref === 1) {
  //         if (data && data.length) {
  //           this.mainTarget.children = data;
  //           for(let global of this.mainTarget.children) this.getTaskChildren(2, global.id);
  //         }
  //       } else if (target_type_ref === 2) {
  //         if (data && data.length) 
  //           for (let global of this.mainTarget.children) 
  //             if (global.id === target_ref) {
  //               global.children = data;
  //               for(let long of global.children) this.getTaskChildren(3, long.id);
  //               break;
  //             } 
  //       } else if (target_type_ref === 3) {
  //         if (data && data.length) 
  //           for (let global of this.mainTarget.children)
  //             for (let long of global.children) {
  //               if (long.id === target_ref) {
  //                 long.children = data;
  //                 break;
  //               }
  //             }
  //       }
  //     }
  //   );
  // }

}
