import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute} from '@angular/router';

import { Target } from '../../models/target';
import { Task } from '../../models/task';
import { Doing } from '../../models/doing';
import { Habit } from '../../models/habit';
import { Period } from '../../models/calendar';

import { ConvertService } from '../../services/convert.service';
import { HttpService } from "../../services/http.service";
import { LoaderService } from '../../services/loader.service';

@Component({
  selector: 'tt-period-report',
  template: `
  <div class="layout-row h100">
    <div *ngIf="period.dateType == 1" class="layout-column flex-50">
      <section class="tt-period-report__doings layout-column flex-50">
        <h4 class="container-title ta-c fw-b">Регулярные дела</h4>
        <div class="flex overflow-a">
          <div class="layout-row">
            <span class="ta-c flex-70">Название</span>
            <span class="ta-c flex-30">Время</span>
          </div>
          <div *ngFor="let doing of regularDoings; let doingIndex = index" class="input-row layout-row layout-align-start-start">
            <span title="{{doing.title}}" class="pv-4 pl-8 flex-70">{{doing.title}}</span>
            <span class="flex-25"><input value="{{regularDoingsTimeString[doingIndex]}}" type="text" (change)="setTime($event, doingIndex)" class="w100 input-bottom" [ngClass]="{'invalid' : doing.spentTime === -1}"></span>
          </div>
        </div>
      </section>
      <section class="tt-period-report__habits layout-column flex-50">
        <h4 class="container-title ta-c fw-b">Привычки</h4>
        <tt-habit-settings class="flex overflow-a" [habits]="habits" [action]="'report'"></tt-habit-settings>
      </section>
    </div>
    <div [ngClass]="{'flex-50':period.dateType == 1, 'flex-100': period.dateType != 1}" class="relative layout-column flex-50">
      <div *ngIf="period.dateType == 1" class="tt-period-report__fulltime">Полное время дня: {{fullTimeString}}</div>
      <section class="tt-period-report__tasks layout-column flex-50">
        <h4 class="container-title ta-c fw-b">Задачи</h4>
        <div class="flex overflow-a"> 
          <div class="layout-row">
            <span class="flex-5"></span>
            <span [ngClass]="{'flex-45':period.dateType != 1,'flex-35':period.dateType == 1}" class="ta-c flex-35">Название</span>
            <span *ngIf="period.dateType == 1" class="ta-c flex-10">Время</span>
            <span class="ta-c flex-35">Комментарий</span>
          </div>
          <div *ngFor="let task of plannedTasks; let taskIndex = index">
            <tt-report-task [task]="task" [period]="period"
                          class="layout-row input-row"
                          (taskReportChanged)="onTaskReportChanged($event)">
            </tt-report-task>
          </div>
        </div>
      </section>
      <section class="tt-period-report__comment layout-column flex-50">
        <button [ngClass]="{'tt-period-report__finish-center':period.dateType != 1}" [disabled]="this.period.dateType == 1 && this.fullTime !== 1440" class="btn btn-primary tt-period-report__finish" (click)="onPeriodReportFinish()"><i class="fa fa-check"></i></button>
        <h4 class="container-title ta-c fw-b">Главная мысль за {{periodName}}</h4>
        <div class="layout-column flex overflow-a">
           <textarea class="flex-100 w100"></textarea>
        </div>
      </section>
    </div>
  </div>
  `,
  styles: []
})
export class PeriodReportComponent implements OnInit, OnDestroy {

  // @Input() period: Period;
  public period: Period;
  private subscription: Subscription;
  private countPeriodLists: number = 0;
  private periodName: string;
  fullTime: number;
  fullTimeString: string;
  plannedTasks: Task[];

  regularDoingsTimeString: string[];
  regularDoings: Doing[];
  habits: Habit[];

  constructor(private convertService: ConvertService, private loaderService: LoaderService, private httpService: HttpService, private activatedRoute: ActivatedRoute) { 
  }

  ngOnInit() {
    this.subscription = this.activatedRoute.queryParams.subscribe( params => {
      this.period = this.convertService.setPeriod(params);
      console.log(this.period);
      switch(this.period.dateType){
        case 1: this.periodName = 'день'; break;
        case 2: this.periodName = 'неделю'; break;
        case 3: this.periodName = 'месяц'; break;
        case 4: this.periodName = 'год'; break;
      }
    });
    if (this.period.dateType == 1) {
      this.getDoingsList();
      this.getHabitsList();
    }
    this.getTasksList();
    
    this.loaderService.onSwitchLoader(false);
    // this.plannedTasks.sort(sortFunction);

    // function sortFunction(a, b){
    //   if((a.isMain && !b.isMain) || (a.isImportant && a.isUrgent && !b.isMain && (!b.isImportant || !b.isUrgent)) || (a.isImportant && !b.isMain && !b.isImportant) || (a.isUrgent && !b.isMain && !b.isImportant && !b.isUrgent))
    //     return -1;
    //   if((b.isMain && !a.isMain) || (b.isImportant && b.isUrgent && !a.isMain && (!a.isImportant || !a.isUrgent)) || (b.isImportant && !a.isMain && !a.isImportant) || (b.isUrgent && !a.isMain && !a.isImportant && !a.isUrgent))
    //     return 1;
    //   return 0;
    // }
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onPeriodReportFinish() {
    // console.log('this.regularDoings: ',this.regularDoings);
    // console.log('this.habits: ',this.habits);
    // console.log('this.plannedTasks: ',this.plannedTasks);

    this.plannedTasks.forEach(task => this.httpService.setDayTasks(task, this.period).subscribe(
      data => {
        console.log(data);
      }
    ))
    if (this.period.dateType == 1) {
      this.regularDoings.forEach(doing => this.httpService.setDayDoings(doing, this.period).subscribe(
        data => {
          console.log(data);
        }
      ))
      this.habits.forEach(habit => this.httpService.setDayHabits(habit, this.period).subscribe(
        data => {
          console.log(data);
        }
      ))
    }

  }

  getPeriodHabits() {
    this.httpService.getPeriodHabits(this.period).subscribe(
      data => {
        console.log(data);
      }
    )
  }
  getPeriodDoings() {
    this.httpService.getPeriodDoings(this.period).subscribe(
      data => {
        console.log(data);
      }
    )
  }

  getHabitsList() {
    this.httpService.getHabitsList().subscribe(
      data => {
        this.getPeriodHabits();
        this.habits = data.habitss;
        this.countPeriodLists++;
        if (this.countPeriodLists == 3) this.countFullTime();
      }
    )
  }
  getDoingsList() {
    this.httpService.getDoingsList().subscribe(
      data => {
        this.getPeriodDoings();
        this.regularDoings = data.doings;
        this.regularDoingsTimeString = new Array(this.regularDoings.length);
        this.regularDoings.forEach((doing, index) => this.regularDoingsTimeString[index] = this.convertService.setTimeString(doing.idealTime));
        this.countPeriodLists++
        if (this.countPeriodLists == 3) this.countFullTime();
      }
    )
  }
  getTasksList() {
    this.httpService.getTasksList(this.period).subscribe(
      data => {
        console.log(data);
        this.plannedTasks = data.tasks;
        this.countPeriodLists++
        if (this.countPeriodLists == 3) this.countFullTime();
        // this.regularDoingsTimeString = new Array(this.regularDoings.length);
        // this.regularDoings.forEach((doing, index) => this.regularDoingsTimeString[index] = this.convertService.setTimeString(doing.idealTime))
      }
    )
  }

  countFullTime() {
    if (this.period.dateType != 1) return;

    this.fullTime = 0;
    console.log()
    this.plannedTasks.forEach(task => this.fullTime += task.spentTime ? task.spentTime : 0);
    this.regularDoings.forEach(doing => this.fullTime += doing.spentTime ? doing.spentTime : doing.idealTime ? doing.idealTime : 0);
    this.fullTimeString = this.convertService.setTimeString(this.fullTime);
  }

  setTime(e, doingIndex) {
    this.regularDoings[doingIndex].spentTime = this.convertService.setTime(e.target.value);
    this.countFullTime();
  }

  onTaskReportChanged(task: Task) {
    this.countFullTime();
  }

}
