import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute} from '@angular/router';

import { Target } from '../../models/target';
import { Task } from '../../models/task';
import { Doing } from '../../models/doing';
import { Habit } from '../../models/habit';
import { Period } from '../../models/calendar';
import { ColumnChart } from '../../models/highcharts';

import { HttpService } from "../../services/http.service";
import { LoaderService } from '../../services/loader.service';
import { ConvertService } from '../../services/convert.service';
import { AccountService } from '../../services/account.service';

@Component({
  selector: 'tt-period-stat',
  template: `
  <section class="layout-row">
    <section class="layout-column flex-50">
      <section class="tt-routine-settings__section flex-50">
        <h4 class="container-title">График рутины</h4>  
        <tt-highcharts [type]="'doings'" *ngIf="doings && doingsOptions.doingsNames.length == doings.length" [options]="doingsOptions"></tt-highcharts>
      </section>
      <section class="tt-routine-settings__section flex-50">
        <h4 class="container-title">График привычек</h4>  
        <tt-highcharts [type]="'column'" [options]="null"></tt-highcharts>
      </section>
    </section>
    <section class="layout-column flex-50">
    </section>
  </section>
  `,
  styles: []
})
export class PeriodStatComponent implements OnInit {

  private mainTarget: Target;
  private subscription: Subscription;
  public period: Period;
  private countPeriodLists: number = 0;
  private habits: Habit[];
  private doings: Doing[];
  private doingsOptions: ColumnChart = {doingsNames: [],idealDoingsTime: [],realDoingsTime: []};
  private tasks: Task[];

  constructor(private convertService: ConvertService, private accountService: AccountService, private loaderService: LoaderService, private httpService: HttpService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.queryParams.subscribe( params => this.period = this.convertService.setPeriod(params));
    this.accountService.getMainTarget(0).then((mainTarget: Target) => {
      this.mainTarget = mainTarget;
      this.countCharts();
    });
    this.getPeriodHabits();
    this.getPeriodDoings();
    this.getTasksList();
  }

  getPeriodHabits() {
    this.httpService.getPeriodHabits(this.period).subscribe(
      data => {
        this.habits = data.habitss;
        this.countCharts();
      }
    )
  }
  getPeriodDoings() {
    this.httpService.getPeriodDoings(this.period).subscribe(
      data => {
        this.doings = data.doings;
        data.doings.forEach(doing => {
          let sum = 0
          doing.periods.forEach(period => sum += period.elapsed);
          this.doingsOptions.realDoingsTime.push(Math.ceil(sum / doing.periods.length));
          this.doingsOptions.idealDoingsTime.push(doing.idealTime);
          this.doingsOptions.doingsNames.push(doing.title);
        });
        this.countCharts();
      }
    )
  }
  getTasksList() {
    this.httpService.getTasksList(this.period).subscribe(
      data => {
        this.tasks = data.tasks;
        this.countCharts();
      }
    )
  }

  countCharts() {
    this.countPeriodLists++;
    if (this.countPeriodLists == 4) this.loaderService.onSwitchLoader(false);      
  }

}
