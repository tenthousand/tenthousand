import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute} from '@angular/router';

import { Period } from '../../models/calendar';
import { PERIOD_ROUTES } from './period.routes';

import { ConvertService } from '../../services/convert.service';

@Component({
  selector: 'tt-period',
  template: `
    <nav class="tt-period-nav layout-row layout-align-center-center ta-c tabs">
      <div class="tt-period-date">
        <span class="tt-period-date-start">
          <span *ngIf="period.firstDay">{{period.startDate | date: 'dd MMMM yyyy'}}</span>
          <span *ngIf="!period.firstDay && period.month !== undefined">{{period.startDate | date: 'MMMM yyyy'}}</span>
          <span *ngIf="period.month === undefined">{{period.startDate | date: 'yyyy'}}</span>
        </span>
        <span *ngIf="period.startDate !== undefined && period.endDate !== undefined && period.lastDay" class="tt-period-date-start">
           - 
          <span>{{period.endDate | date: 'dd MMMM yyyy'}}</span>
        </span>
      </div>
      <a class="mb-4 tt-period-nav__button flex-15 tabs-button" (click)="onNavButtonClick('plan')">Планирование</a>
      <a class="mb-4 tt-period-nav__button flex-15 tabs-button" (click)="onNavButtonClick('report')">Отчет</a>
      <a class="mb-4 tt-period-nav__button flex-15 tabs-button" (click)="onNavButtonClick('stat')">Статистика</a>
    </nav>
    <main class="tt-period-main">
      <router-outlet></router-outlet>
    </main>
  `
})

      // <section class="tt-period-section" *ngIf="activeTab === 0">
      //   <tt-period-plan [period]="period"></tt-period-plan>
      // </section>
      // <section class="tt-period-section" *ngIf="activeTab === 1">
      //   <tt-period-report [period]="period" class="layout-row"></tt-period-report>
      // </section>
      // <section class="tt-period-section" *ngIf="activeTab === 2">
      //   <tt-period-stat></tt-period-stat>
      // </section>


      // <div [ngClass]="{'active':activeTab == 0}" class="mb-4 tt-period-nav__button flex-15 tabs-button" (click)="onNavButtonClick(0)">Планирование</div>
      // <div [ngClass]="{'active':activeTab == 1}" class="mb-4 tt-period-nav__button flex-15 tabs-button" (click)="onNavButtonClick(1)">Отчет</div>
      // <div [ngClass]="{'active':activeTab == 2}" class="mb-4 tt-period-nav__button flex-15 tabs-button" (click)="onNavButtonClick(2)">Статистика</div>
    

export class PeriodComponent implements OnInit, OnDestroy {

  public period: Period;
  private subscription: Subscription;
  public activeTab: number = 0;
  private queryParams: Object;

  constructor(private router: Router, private convertService: ConvertService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.subscription = this.activatedRoute.queryParams.subscribe( params => this.setPeriod(params));
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  setPeriod(params) {
    this.queryParams = params;
    this.period = this.convertService.setPeriod(params);
  }

  onNavButtonClick(state) {
    // this.activeTab = num;
    this.router.navigate(['/period/'+state], {queryParams: this.queryParams});
  }

}
