import { Component,OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { AccountService } from '../../services/account.service';
import { LoaderService } from '../../services/loader.service';

@Component({
  selector: 'tt-main',
  template: `
    <div class="tt-main-image"></div>
    <main class="layout-column layout-align-center-center tt-main-container">
      <section [ngSwitch]="mainFraseNumber" class="tt-main-frase">
        <p *ngSwitchCase="0">Тебе не понравится.<br/> Но ты не пожалеешь</p>
        <p *ngSwitchCase="1">Времени у тебя нет.<br/> А выбора нет вовсе</p>
        <p *ngSwitchCase="2">Никогда не сдавайся</p>
        <p *ngSwitchCase="3">Самурай принимает решение<br/>в течение семи вдохов и выдохов</p>
        <p *ngSwitchCase="4">Тот, у кого нет "завтра",<br/>не может быть робким</p>
      </section>
      <button class="btn btn-primary" (click)="startPath()">Начать</button>
    </main>
  `
})
export class MainComponent implements OnInit {

  public isAuthorized: boolean;

  public mainFraseNumber: number = Math.floor(Math.random() * 5);

  constructor(private router: Router, private accountService: AccountService, private loaderService: LoaderService) {}

  startPath(){
    if (!this.isAuthorized) this.router.navigate(['/account']);
    else this.router.navigate(['/targets']);
  }

  ngOnInit() {
      this.accountService.onAuthorisationChanges.subscribe(
          isAuthorized => this.isAuthorized = isAuthorized
      );
      this.loaderService.onSwitchLoader(false);
  }

}
