import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';

import { HttpService } from "../../services/http.service";

@Component({
  selector: 'tt-popup',
  template: `
  <section [ngClass]="{'op-0':initialStyles}" class="tt-popup__background"></section>
  <section [ngSwitch]="type" [ngClass]="{'top0':initialStyles, 'op-0':initialStyles}" class="tt-popup__content">
    <div *ngSwitchCase="'targetEdit'" class="tt-popup__targetedit">
      <tt-popup-targetedit [target]="content"></tt-popup-targetedit>
    </div>
    <div class="close fa fa-close tt-popup__close" (click)="onPopupClose()"></div>
  </section>
  `,
  styles: []
})
export class PopupComponent implements OnInit, AfterViewInit {
  
  @Input() type: string;
  @Input() content: any;
  @Output() closePopup = new EventEmitter<boolean>();

  initialStyles: boolean = true;

  constructor(private httpService: HttpService) {}

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout(() => this.initialStyles = false)
  }

  onPopupClose() {
    this.initialStyles = true;
    setTimeout(() => { this.closePopup.emit() }, 500);
  }

}
