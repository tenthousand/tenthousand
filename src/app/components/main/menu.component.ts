import { Component } from '@angular/core';

import { AccountService } from '../../services/account.service';

@Component({
  selector: 'tt-menu',
  template: `
    <section class="tt-menu__left-side layout-row layout-align-start-center">
      <div class="tt-menu__link">
        <div ttDropdown class="tt-menu__dropdown dropdown">
          <span class="tt-menu__link-button">Цели</span>
          <nav>
            <a class="tt-menu__link-button" [routerLink]="['/targets/settings']">Настройка</a>
            <a class="tt-menu__link-button" [routerLink]="['/targets/overview']">Обзор</a>
          </nav>
        </div>
        
      </div>
      <div class="tt-menu__link">
        <div ttDropdown class="tt-menu__dropdown dropdown">
          <span class="tt-menu__link-button">Рутина</span>
          <nav>
            <a class="tt-menu__link-button" [routerLink]="['/routine/settings']">Настройка</a>
          </nav>
        </div>
      </div>
      <div class="tt-menu__link">
        <a class="tt-menu__link-button" [routerLink]="['/calendar']">Календарь</a>
      </div>
    </section>
    <section class="tt-menu__right-side layout-row layout-align-end-center">
      <div class="tt-menu__link">
        <div ttDropdown class="tt-menu__dropdown dropdown">
          <span class="tt-menu__link-button">Профиль</span>
          <nav>
            <p (click)="logOut()">Выйти</p>
          </nav>
        </div>
      </div>
    </section>
  `,
  styles: []
})
export class MenuComponent {

  constructor(private accountService: AccountService) {}

  logOut () {
    this.accountService.onLogOut();
  }

}
