import { Component } from '@angular/core';

@Component({
  selector: 'tt-targets',
  template: `
    <router-outlet></router-outlet>
  `,
  styles: []
})
export class TargetsComponent {

  constructor() { }

}
