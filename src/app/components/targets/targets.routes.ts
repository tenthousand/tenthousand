import { Routes } from "@angular/router";

import { TargetsSettingsComponent } from "./targets-settings.component";
import { TargetsOverviewComponent } from "./targets-overview.component";

export const TARGETS_ROUTES: Routes = [
    { path: 'settings', component: TargetsSettingsComponent },
    { path: 'overview', component: TargetsOverviewComponent }
];
