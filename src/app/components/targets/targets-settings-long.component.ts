import { Component, Input, OnInit, OnChanges, EventEmitter, Output  } from '@angular/core';

import { Target } from '../../models/target';

import { HttpService } from "../../services/http.service";

@Component({
  selector: 'tt-targets-settings-long',
  template: `
    <div ttDropdown class="tt-targets-settings__long-dropdown transition-5 tt-targets-settings__long" [ngClass]="{'op-1':targetsSettingsStep != 0}">
      <section class="relative">
        <input (blur)="saveTarget()" class="transition-5 ta-c tt-targets-settings__long-input" type="text" [disabled]="targetsSettingsStep > 2" (keyup)="onLongTagetInputChanged()" [(ngModel)]="long.target_name">
        <i *ngIf="targetsSettingsStep > 2" [ngClass]="{'checked':statusLongTarget}" class="fa fa-check tt-targets-settings__long-check"></i>
      </section>
      <section [ngClass]="{'disp-n':targetsSettingsStep < 3}" class="tt-targets-settings__long-short-wrapper">
          <tt-targets-settings-short 
            *ngFor="let short of long.children"
            (changeStatusShortTarget)="onChangeStatusShortTarget($event)"
            [targetsSettingsStep]="targetsSettingsStep"
            [short]="short">
          </tt-targets-settings-short>
        <hr>
      </section>
    </div>
  `
})
export class TargetsSettingsLongComponent implements OnInit, OnChanges {

  public targetId: string;
  @Input() long: Target;
  @Input() targetsSettingsStep: number;

  @Output() changeStatusLongTarget = new EventEmitter<boolean>();

  public filledShortTargets: number = 0;
  public statusLongTarget: boolean = false;

  constructor(private httpService: HttpService) { }

  ngOnInit() {
    this.long.children = [/*{title:'',target_type_ref:3}, {title:'',target_type_ref:3}*/];
  }

  ngOnChanges (changes) {
    if (changes.targetsSettingsStep.currentValue == 3) {
      this.filledShortTargets = 0;
      this.statusLongTarget = false;
    } else if (changes.targetsSettingsStep.currentValue == 2 && this.long.target_name.length > 0) {
      this.statusLongTarget = true;
    }
  }

  onLongTagetInputChanged() {
    if (this.long.target_name.length > 0) {
      if(this.statusLongTarget != true) {
        this.statusLongTarget = true;
        this.changeStatusLongTarget.emit(true); 
      }
    } else {
      if (this.long.id) {
        this.httpService.deleteTarget(this.long.id).subscribe(data => console.log(data));
        this.long.id = undefined;
      }
      if(this.statusLongTarget != false) {
        this.statusLongTarget = false;
        // console.log('emited false');
        this.changeStatusLongTarget.emit(false); 
      }
    }
  }

  onChangeStatusShortTarget(status: boolean) {
    if (status) this.filledShortTargets += 1;
    else this.filledShortTargets -= 1;
    if (this.filledShortTargets === this.long.children.length) this.long.children.push({target_name:'',target_type_ref:3,parent_ref:this.long.id});
    else if (this.long.children.length > 2 && this.filledShortTargets < this.long.children.length) {
      for (let g in this.long.children) {
        let targetIndex = this.long.children.length - (parseInt(g) + 1);
        if (this.long.children[targetIndex].target_name.length === 0){
          if (this.long.children[targetIndex].id) 
            this.httpService.deleteTarget(this.long.children[targetIndex].id).subscribe(/*data => console.log(data)*/);
          this.long.children.splice(targetIndex, 1);
          break;
        }
      }
    }
    if (this.filledShortTargets >= 2 && this.filledShortTargets == this.long.children.length - 1) {
      if (this.statusLongTarget != true) {
        this.statusLongTarget = true;
        this.changeStatusLongTarget.emit(true); 
      }
    } else {
      if (this.statusLongTarget == true) {
        this.statusLongTarget = false;
        // console.log('emited false');
        this.changeStatusLongTarget.emit(false); 
      }
    }
  }

  saveTarget() {
    if(this.long.target_name.length > 1) {
      console.log(this.long);
      this.httpService.saveTarget(this.long).subscribe(
          data => {
            console.log(data);
            if (data) {
                this.long.id = data.id;
                this.targetId = data.id;
                if(this.long.children.length === 0 && this.long.id)
                  this.long.children = [{target_name:'',target_type_ref:3,parent_ref:this.long.id}, {target_name:'',target_type_ref:3,parent_ref:this.long.id}];
              }
            }
        );
    }
  }

}
