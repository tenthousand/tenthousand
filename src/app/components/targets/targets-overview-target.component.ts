import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Target } from '../../models/target';

@Component({
  selector: 'tt-targets-overview-target',
  template: `
    <div class="disp-ib h100 va-m"></div>
    <div class="tt-targets-overview-target disp-ib va-m w90" [ngClass]="{'tt-targets-overview-target__shown':target.showChildren}">
      <div class="layout-row w100">
        <span (click)="onEditTarget()" class="flex-5 tt-targets-overview-target__button tt-targets-overview-target__edit"><i class="fa fa-pencil"></i></span>
        <span class="flex-5 tt-targets-overview-target__button tt-targets-overview-target__description" title="{{target.description}}"><i class="fa fa-lightbulb-o"></i></span>
        <span class="flex-50 tt-targets-overview-target__text">
          <span class="tt-targets-overview-target__name cursor-p disp-ib">{{target.target_name}}</span>
        </span>
        <span class="flex-35 tt-targets-overview-target__text tt-targets-overview-target__deadline cursor-p">{{target.deadline}}</span>
        <span *ngIf="target.target_type_ref != 3" class="flex-5 tt-targets-overview-target__button tt-targets-overview-target__toggle cursor-p" (click)="onTargetToggle()"><i [ngClass]="{'fa-caret-down':!isChildrenOpened,'fa-caret-up':isChildrenOpened}" class="fa fa-caret-down"></i></span>
      </div>
    </div>
  `,
  styles: []
})
export class TargetsOverviewTargetComponent implements OnInit {

  @Input() target: Target;
  @Output() targetToggle = new EventEmitter<boolean>();
  @Output() targetEdit = new EventEmitter();

  isChildrenOpened: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  onEditTarget() {
    this.targetEdit.emit();
  }

  onTargetToggle() {
    this.isChildrenOpened = !this.isChildrenOpened;
    this.targetToggle.emit(this.isChildrenOpened);
  }

}
