import { Component, AfterViewInit, ElementRef, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { Target } from '../../models/target';
import { SiteTour } from '../../models/site-tour';

import { HttpService } from "../../services/http.service";
import { LoaderService } from '../../services/loader.service';
@Component({
  selector: 'tt-targets-settings',
  template: `
  <div class="tt-targets">
    <div class="layout-column layout-align-space-between-center h100" [ngClass]="{'tt-targets-settings__options-main-ok':targetsSettingsStep == 1, 
                    'tt-targets-settings__options-global-ok':targetsSettingsStep == 2,
                    'tt-targets-settings__options-long-ok':targetsSettingsStep == 3}">
      <section class="w100 layout-column flex-95 layout-align-start-center">
        <div class="tt-targets-settings__main transition-5 layout-column layout-align-center-center" 
                 [ngClass]="{'h100':targetsSettingsStep == 0}">
          <tt-sitetour [ngClass]="{'disp-n':!siteTour.show}"
                       [siteTour]="siteTour" 
                       (changeShow)="siteTour.show = $event">
          </tt-sitetour>
          <input (blur)="saveTarget()" class="ta-c transition-5 tt-targets-settings__main-input" [disabled]="targetsSettingsStep != 0" (keyup)="onMainTagetInputChanged($event)" type="text" [(ngModel)]="mainTarget.target_name">
        </div>
        <div [ngClass]="{'disp-n':targetsSettingsStep == 0}" class="w100 layout-wrap layout-row">
          <div [ngClass]="{'flex-25':mainTarget.children.length > 3,'flex-33':mainTarget.children.length == 3,'flex-50':mainTarget.children.length == 2}"
            class="flex-25 p-8" *ngFor="let global of mainTarget.children; let i = index">
            <tt-targets-settings-global
              class="tt-targets-settings__column"
              (changeStatusGlobalTarget)="onChangeStatusGlobalTarget($event)"
              [global]="global"
              [targetsSettingsStep]="targetsSettingsStep">
            </tt-targets-settings-global>
          </div>
        </div>
      </section>
      <section class="w100 flex-10 layout-row layout-align-end-center">
        <button class="flex-15 mr-8 btn btn-default" (click)="targetsSettingsPrevStep()" *ngIf="targetsSettingsStep != 0">Назад</button>
        <button class="flex-15 mr-8 btn btn-primary" (click)="targetsSettingsNextStep()" [disabled]="!canContinueTargetsSettings">{{targetsSettingsStep == 3 ? 'Закончить' : 'Дальше'}}</button>
      </section>
    </div>
  </div>
  `
})
export class TargetsSettingsComponent implements AfterViewInit, OnInit {

  // public targetId: string;
  public filledGlobalTargets: number = 0;
  public canContinueTargetsSettings: boolean = false;
  public targetsSettingsStep: number = 0;
  public mainTarget: Target = { target_name:'', target_type_ref: 0, children: [] };

  constructor(private httpService: HttpService, private router: Router, private loaderService: LoaderService) { }

  ngOnInit () {
    this.getTaskChildren(0);
  }

  ngAfterViewInit () {
    this.siteTourStep(0, 'tt-targets-settings__main-input')
  }

  onMainTagetInputChanged () {
    if (this.mainTarget.target_name.length > 0) this.canContinueTargetsSettings = true;
    else this.canContinueTargetsSettings = false;
  }

  targetsSettingsNextStep () {
    this.targetsSettingsStep += 1; 
    // console.log(this.targetsSettingsStep);
    if (this.targetsSettingsStep === 4) {
      this.router.navigate(['/targets/overview']);
    }
    else if (this.targetsSettingsStep === 3) {
      this.filledGlobalTargets = 0;
      this.canContinueTargetsSettings = false;
    }
    else if (this.targetsSettingsStep === 2) {
      for (let g in this.mainTarget.children) 
          if (this.mainTarget.children[g].target_name.length === 0)
            this.mainTarget.children.splice(parseInt(g), 1);
    } else if (this.targetsSettingsStep === 1)
      this.countFilledGlobalTargets();

    this.siteTour.show = false;
    if (this.targetsSettingsStep == 1 && this.mainTarget.children[0] && this.mainTarget.children[0].target_name.length === 0) {
      this.siteTourStep(1, 'tt-targets-settings__global-input')
    } else if (this.targetsSettingsStep == 2 && this.mainTarget.children[0].children && this.mainTarget.children[0].children[0] && this.mainTarget.children[0].children[0].target_name.length === 0) {
      this.siteTourStep(2, 'tt-targets-settings__long-input')
    } else if (this.targetsSettingsStep == 3 && this.mainTarget.children[0].children[0].children && this.mainTarget.children[0].children[0].children[0] && this.mainTarget.children[0].children[0].children[0].target_name.length === 0) {
      this.siteTourStep(3, 'tt-targets-settings__short-input')
    }

  }
  targetsSettingsPrevStep () { 
    this.filledGlobalTargets = 0;
    this.targetsSettingsStep -= 1; 
    // console.log(this.targetsSettingsStep);
    this.canContinueTargetsSettings = true; 
    if (this.targetsSettingsStep === 1) {
      this.mainTarget.children.push({target_name:'',parent_ref:this.mainTarget.id,target_type_ref:1});
      this.countFilledGlobalTargets();
    }
  }

  countFilledGlobalTargets () {
    this.filledGlobalTargets = 0;
    for (let g of this.mainTarget.children) 
        if (g.target_name.length > 0)
          this.filledGlobalTargets += 1;
    if (this.filledGlobalTargets < 2)
      this.canContinueTargetsSettings = false;
    // console.log(this.filledGlobalTargets);
  }

  onChangeStatusGlobalTarget(status: boolean) {
    // console.log(status);
    if (status) this.filledGlobalTargets += 1;
    else this.filledGlobalTargets -= 1;
    if (this.targetsSettingsStep === 1) {
      // console.log(this.filledGlobalTargets + ' === ' + this.mainTarget.children.length)
      if (this.filledGlobalTargets === this.mainTarget.children.length) this.mainTarget.children.push({target_name:'',parent_ref:this.mainTarget.id,target_type_ref:1});
      else if (this.mainTarget.children.length > 2 && this.filledGlobalTargets < this.mainTarget.children.length)
        for (let g in this.mainTarget.children) {
          let targetIndex = this.mainTarget.children.length - (parseInt(g) + 1);
          if (this.mainTarget.children[targetIndex].target_name.length === 0) {
            if (this.mainTarget.children[targetIndex].id) 
              this.httpService.deleteTarget(this.mainTarget.children[targetIndex].id).subscribe(data => console.log(data));
            this.mainTarget.children.splice(targetIndex, 1);
            break;
          }
        }
      if (this.filledGlobalTargets >= 2 && this.filledGlobalTargets == this.mainTarget.children.length - 1)
        this.canContinueTargetsSettings = true;
      else
        this.canContinueTargetsSettings = false;
    } else if (this.targetsSettingsStep > 1) {
      if (this.filledGlobalTargets >= 2 && this.filledGlobalTargets == this.mainTarget.children.length)
        this.canContinueTargetsSettings = true;
      else
        this.canContinueTargetsSettings = false;
    }
  }

  saveTarget() {
    if(this.mainTarget.target_name.length > 1)
      this.httpService.saveTarget(this.mainTarget).subscribe(
          data => {
            this.mainTarget.id = data.id;
            // this.targetId = data.id;
            if((!this.mainTarget.children || this.mainTarget.children.length === 0) && this.mainTarget.id)
              this.mainTarget.children = [{target_name:'',parent_ref:this.mainTarget.id,target_type_ref:1}, {target_name:'',parent_ref:this.mainTarget.id,target_type_ref:1}];
          }
        );
  }

  getTaskChildren(target_type_ref: number, parent_ref?: string): any {
    this.httpService.getTargetChildren(parent_ref).subscribe(
      data => {
        let longTargetsCount:number = 0;
        if (target_type_ref === 0) {
          if (data.length) {
            this.mainTarget = data[0];
            this.canContinueTargetsSettings = true;
            this.getTaskChildren(1, this.mainTarget.id);
          } else
              this.loaderService.onSwitchLoader(false);
        } else if (target_type_ref === 1) {
          if (data.length) {
            // console.log(data);
            this.mainTarget.children = data;
            for(let global of this.mainTarget.children) {
              this.getTaskChildren(2, global.id);
              if (global.id == this.mainTarget.children[this.mainTarget.children.length - 1].id)
                this.loaderService.onSwitchLoader(false);
            }
            this.mainTarget.children.push({target_name:'',parent_ref:this.mainTarget.id,target_type_ref:1});
          } else this.mainTarget.children = [{target_name:'',parent_ref:this.mainTarget.id,target_type_ref:1}, {target_name:'',parent_ref:this.mainTarget.id,target_type_ref:1}];
        } else if (target_type_ref === 2) {
          if (data.length) 
            for (let global of this.mainTarget.children) 
              if (global.id === parent_ref) {
                global.children = data;
                for(let long of global.children) this.getTaskChildren(3, long.id);
                global.children.push({target_name:'',parent_ref:global.id,target_type_ref:2});
                break;
              }
          else 
              for (let global of this.mainTarget.children)
                if (global.id === parent_ref) 
                  global.children = [{target_name:'',parent_ref:global.id,target_type_ref:2}, {target_name:'',parent_ref:global.id,target_type_ref:2}];
        } else if (target_type_ref === 3) {
          if (data.length) 
            for (let global of this.mainTarget.children)
              for (let long of global.children) {
                if (long.id === parent_ref) {
                  long.children = data;
                  long.children.push({target_name:'',parent_ref:long.id,target_type_ref:3});
                  break;
                }
              }
          else 
            for (let global of this.mainTarget.children)
              for (let long of global.children) {
                if (long.id === parent_ref) {
                  long.children = [{target_name:'',parent_ref:long.id,target_type_ref:3}, {target_name:'',parent_ref:long.id,target_type_ref:3}];
                }
              }
        }
      }
    );
  }

  private siteTourSteps: SiteTour[] = [{
    title: 'Выбор основной цели',
    description: `Сейчас вам нужно указать свою основную цель. Определить эту цель очень легко - она должна быть квинтессенцией ваших жизненных ценностей. Да, определить жизненные ценности куда сложнее. Немногие люди задумываются о них. Но именно сейчас настало время вам определиться, что для вас самое важное в жизни. Это может быть материальный достаток. Благополучная семья. Слава. Власть. Познание. Служение во имя человечества… Без чего вы не можете представить свою жизнь? Когда вы определитесь с жизненными ценностями, вам нужно выбрать глобальную цель. Нечто такое, что станет вашим памятником. Что послужит примером вашим потомкам и наследникам. Цель, достойная вас. Цель, ради которой вы пройдете долгий путь в 10000 часов.\b\n`,
    show: true
  },{
    title: 'Определить глобальные цели',
    description: `Это небольшой список по-настоящему колоссальных целей, которые необходимо выполнить, чтобы добиться вашей глобальной цели. Это просто список из нескольких пунктов. Когда этот список будет полность выполнен, это будет означать, что и ваша основная цель - достигнута, или для её достижения остались считанные недели. Например, если ваша цель - руководить отделом поиска в Google, то вашими глобальными целями могут быть овладение английским как родным языком, разработка собственной поисковой системы и опыт работы в Google. В принципе, это будет значить, что вы вполне способны добиться вашей основной цели. Основную цель необходимо разбить минимум на две глобальные.`,
    show: true
  },{
    title: 'Определить долгосрочные цели',
    description: `Каждая такая цель ставится на три-четыре года вперёд. Важно указывать, для достижения какой именно глобальной цели вы пишите каждую из задач. Например, чтобы стать носителем английского языка, вам нужно получить соответствующий сертификат и переехать в англоговорящую страну. Принцип тот же - если вы добились всех долгосрочных целей для глобальной цели - значит, фактически эта цель достигнута. Каждую глобальную цель необходимо разбить минимум на две долгосрочные.`,
    show: true
  },{
    title: 'Определить краткосрочные цели ',
    description: `Что вы можете сделать в ближайшие недели или месяцы, чтобы достичь той или этой цели? Больше 4-х месяцев на краткосрочную цель лучше не закладывать, а то исчезает смысл части "кратко-". Эти цели должны быть реализуемы в ближайшее время. Например, чтобы получить сертификат носителя языка, для начала можно пройти курс по языку, соответствующий вашему нынешнему уровню. Каждую долгосрочную цель необходимо разбить минимум на две краткосрочные. Чтобы указывать краткосрочные цели, кликайте на свои долгосрочные цели, вы увидите поля для ввода.`,
    show: true
  },]
  private siteTour: SiteTour = this.siteTourSteps[0];

  siteTourStep (step: number, elementName: string) {
    let that = this;
    setTimeout(() => {
      that.siteTour = that.siteTourSteps[step];
      /*let sitetourArray: any = document.getElementsByTagName('tt-sitetour');
      console.log(sitetourArray);
      let sitetour = sitetourArray[0];
      let sitetourElement: any = document.getElementsByClassName(elementName)[0];
      sitetourArray[0].classList.remove('disp-n');
      let sitetourPop = {
        height: sitetour.clientHeight,
        width: sitetour.clientWidth
      }
      let sitetourElementParams = {
        top: sitetourElement.offsetTop,
        left: sitetourElement.offsetLeft,
        width: sitetourElement.offsetWidth
      }
      console.log(sitetourPop)
      console.log(sitetourElementParams)
      sitetour.style.top = (sitetourElementParams.top - sitetourPop.height - 12)+'px';
      sitetour.style.left = 
        ((sitetourElementParams.left + ((sitetourElementParams.width / 2) - sitetourPop.width / 2 )))+'px';*/
    },600)
  }

}
