import { Component, OnInit } from '@angular/core';

import { Target } from '../../models/target';

import { HttpService } from "../../services/http.service";
import { LoaderService } from '../../services/loader.service';

@Component({
  selector: 'tt-targets-overview',
  template: `
  <tt-popup *ngIf="popupView" [type]="'targetEdit'" [content]="edtitingTarget" (closePopup)="onClosePopup()"></tt-popup>
  <div class="layout-row tt-targets">
    <ul class="flex-100 tt-targets-overview-ul" *ngIf="mainTarget && mainTarget.children && mainTarget.children.length > 0">
      <li class="layout-row w100 relative tt-targets-overview-ul-li" *ngFor="let global of mainTarget.children; let g = index">
        <tt-targets-overview-target class="w33 ta-c" (targetEdit)="onTargetEdit(global)" (targetToggle)="onTargetToggle(global, $event)" [target]="global"></tt-targets-overview-target>
        <ul class="flex-66 tt-targets-overview-ul left100" *ngIf="global.showChildren">
          <li class="layout-row w100 relative tt-targets-overview-ul-li" *ngFor="let long of global.children; let l = index">
            <tt-targets-overview-target class="w50 ta-c" (targetEdit)="onTargetEdit(long)" (targetToggle)="onTargetToggle(long, $event)" [target]="long"></tt-targets-overview-target>
            <ul class="flex-50 tt-targets-overview-ul left100" *ngIf="long.showChildren">
              <li class="layout-row w100 tt-targets-overview-ul-li" *ngFor="let short of long.children; let s = index">
                <tt-targets-overview-target class="w100 ta-c" (targetEdit)="onTargetEdit(short)" (targetToggle)="onTargetToggle(short, $event)" [target]="short"></tt-targets-overview-target>
              </li>
            </ul>
          </li>
        </ul>
      </li>
    </ul>
  </div>
  `,
  styles: []
})
export class TargetsOverviewComponent implements OnInit {

  public popupView: boolean = false;
  public mainTarget: Target;
  public edtitingTarget: Target;
  private targetsCount: number = 0;

  constructor(private httpService: HttpService, private loaderService: LoaderService) { }

  ngOnInit() {
    this.getTaskChildren(0);
  }

  onClosePopup(){
    this.popupView = false;
  }
  onTargetEdit(target) {
    this.edtitingTarget = target;
    this.popupView = true;
  }

  onTargetToggle(target, openChildren) {
    target.showChildren = openChildren;
  }

  getTaskChildren(target_type_ref: number, parent_ref?: string): any {
    this.httpService.getTargetChildren(parent_ref).subscribe(
      data => {
        if (data) {
          if (target_type_ref === 0) {
            if (data.length) {
              this.mainTarget = data[0];
              this.mainTarget.target_type_ref = 0;
              this.getTaskChildren(1, this.mainTarget.id);
            } else 
                this.loaderService.onSwitchLoader(false);
            
          } else if (target_type_ref === 1) {
            if (data.length) {
              data = data.map(item => {item.target_type_ref = 1; return item});
              this.mainTarget.children = data;
              for(let global of this.mainTarget.children) this.getTaskChildren(2, global.id);
            } else 
                this.loaderService.onSwitchLoader(false);
          } else if (target_type_ref === 2) {
            if (data.length) {
              for (let global of this.mainTarget.children) {
                if (global.id === parent_ref) {
                  data = data.map(item => {item.target_type_ref = 2; return item});
                  global.children = data;
                  for(let long of global.children) this.getTaskChildren(3, long.id);
                  break;
                } }
            } else 
                this.loaderService.onSwitchLoader(false);
          } else if (target_type_ref === 3) {
            if (data.length) {
              for (let global of this.mainTarget.children)
                for (let long of global.children) {
                  if (long.id === parent_ref) {
                    if (long.id == global.children[global.children.length - 1].id) {
                      this.targetsCount++;
                      console.log(this.targetsCount + ' == ' + global.children.length);
                      if (this.targetsCount == global.children.length) 
                        this.loaderService.onSwitchLoader(false);
                    }
                    data = data.map(item => {item.target_type_ref = 3; return item});
                    long.children = data;
                    break;
                  }
                }
            } else 
                this.loaderService.onSwitchLoader(false);
          }
        }
      }
    );
  }

}
