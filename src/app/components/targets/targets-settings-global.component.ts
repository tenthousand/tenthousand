import { Component, Input, OnInit, EventEmitter, Output, OnChanges } from '@angular/core';

import { Target } from '../../models/target';

import { HttpService } from "../../services/http.service";

@Component({
  selector: 'tt-targets-settings-global',
  template: `
    <div class="transition-5 tt-targets-settings__global" [ngClass]="{'op-1':targetsSettingsStep != 0}">
      <section>
        <input (blur)="saveTarget()" class="transition-5 ta-c tt-targets-settings__global-input" type="text" [disabled]="targetsSettingsStep > 1" (keyup)="onGlobalTagetInputChanged()" [(ngModel)]="global.target_name">
      </section>
      <section [ngClass]="{'disp-n':targetsSettingsStep < 2}" class="tt-targets-settings__global-long-wrapper">
          <tt-targets-settings-long 
            (changeStatusLongTarget)="onChangeStatusLongTarget($event)"
            [targetsSettingsStep]="targetsSettingsStep"
            *ngFor="let long of global.children"
            [long]="long">
          </tt-targets-settings-long>
      </section>
    </div>
  `,
  styles: []
})
export class TargetsSettingsGlobalComponent implements OnInit, OnChanges {

  // public targetId: string;
  public filledLongTargets: number = 0;
  public statusGlobalTarget: boolean;
  @Output() changeStatusGlobalTarget = new EventEmitter<boolean>();

  @Input() global: Target;
  @Input() targetsSettingsStep: number;
  constructor(private httpService: HttpService) { }

  ngOnInit () {
    this.global.children = [/*{title:'',target_type_ref:2}, {title:'',target_type_ref:2}*/];
  }

  ngOnChanges (changes) {
    if (this.targetsSettingsStep === 3) {
      for (let g in this.global.children) 
          if (this.global.children[g].target_name.length === 0)
            this.global.children.splice(parseInt(g), 1);
      this.filledLongTargets = 0;
      this.statusGlobalTarget = false;
    } else if (this.targetsSettingsStep == 2) {
      this.countFilledLongTargets(changes.targetsSettingsStep.previousValue);
      if (this.filledLongTargets == this.global.children.length){
        this.global.children.push({target_name:'',target_type_ref:2, parent_ref:this.global.id});
      }
    } else if (changes.targetsSettingsStep.currentValue == 1 && this.global.target_name.length > 0) {
        this.statusGlobalTarget = true;
    }
  }
  
  countFilledLongTargets (previousValue: number) {
    this.filledLongTargets = 0;
    for (let g in this.global.children) 
        if (this.global.children[g].target_name.length > 0)
          this.filledLongTargets += 1;
    if (this.filledLongTargets < 2){
      this.statusGlobalTarget = false;
      this.changeStatusGlobalTarget.emit(false);
    } else {
      if (this.statusGlobalTarget != true || previousValue === 3) {
        this.statusGlobalTarget = true;
        this.changeStatusGlobalTarget.emit(true);
      }
    }
  }

  onGlobalTagetInputChanged() {
    // console.log(this.global.target_name+': '+this.global.target_name.length);
    if (this.global.target_name.length > 0) {
      if(this.statusGlobalTarget != true) {
        this.statusGlobalTarget = true;
        this.changeStatusGlobalTarget.emit(true); 
      }
    } else {
      if (this.global.id) {
        this.httpService.deleteTarget(this.global.id).subscribe(data => console.log(data));
        this.global.id = undefined;
      }
      if (this.statusGlobalTarget != false) {
        this.statusGlobalTarget = false;
        // console.log('emited false');
        this.changeStatusGlobalTarget.emit(false); 
      }
    }
  }

  onChangeStatusLongTarget(status: boolean) {
    if (status) this.filledLongTargets += 1;
    else this.filledLongTargets -= 1;
    if (this.targetsSettingsStep === 2) {
      if (this.filledLongTargets === this.global.children.length) this.global.children.push({target_name:'',target_type_ref:2, parent_ref:this.global.id});
      else if (this.global.children.length > 2 && this.filledLongTargets < this.global.children.length) {
        for (let g in this.global.children) {
          let targetIndex = this.global.children.length - (parseInt(g) + 1);
          if (this.global.children[targetIndex].target_name.length === 0){
            if (this.global.children[targetIndex].id) 
              this.httpService.deleteTarget(this.global.children[targetIndex].id).subscribe(/*data => console.log(data)*/);
            this.global.children.splice(targetIndex, 1);
            break;
          }
        }
      }
    }
    let comparable = this.targetsSettingsStep == 2 ? this.global.children.length - 1 : this.global.children.length;
    if (this.filledLongTargets >= 2 && this.filledLongTargets == comparable) {
      if (this.statusGlobalTarget != true) {
        this.statusGlobalTarget = true;
        this.changeStatusGlobalTarget.emit(true); 
      }
    } else {
      if (this.statusGlobalTarget == true) {
        this.statusGlobalTarget = false;
        this.changeStatusGlobalTarget.emit(false); 
      }
    }
  }

  saveTarget() {
    // console.log(this.global.parent_ref);
    if(this.global.target_name.length > 1)
      this.httpService.saveTarget(this.global).subscribe(
          data => {
            this.global.id = data.id;
            // this.targetId = data.id;
            // console.log(this.targetId);
            if(this.global.children.length === 0 && this.global.id)
              this.global.children = [{target_name:'',target_type_ref:2,parent_ref:this.global.id}, {target_name:'',target_type_ref:2,parent_ref:this.global.id}];
          }
        );
  }

}
