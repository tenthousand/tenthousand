import { Component, Input, OnChanges, EventEmitter, Output } from '@angular/core';
 
import { Target } from '../../models/target';

import { HttpService } from "../../services/http.service";

@Component({
  selector: 'tt-targets-settings-short',
  template: `
    <section class="transition-5 tt-targets-settings__short-input-container">
      <input (blur)="saveTarget()" class="transition-5 ta-c tt-targets-settings__short-input" type="text" (keyup)="onShortTagetInputChanged()" [(ngModel)]="short.target_name">
    </section>
  `,
  styles: []
})
export class TargetsSettingsShortComponent implements OnChanges {
 
  public targetId: string;
  @Input() short: Target;
  @Input() targetsSettingsStep: number;

  @Output() changeStatusShortTarget = new EventEmitter<boolean>();

  public statusShortTarget: boolean = false;

  constructor(private httpService: HttpService) { }

  ngOnChanges (changes) {
    if (changes.targetsSettingsStep.currentValue == 3) {
      if (this.short.target_name.length > 0) this.setAndSendChanges(true);
    }
  }

  onShortTagetInputChanged() {
    if (this.short.target_name.length > 0) {
      if(this.statusShortTarget != true) this.setAndSendChanges(true);
    } else {
      if (this.short.id) {
        this.httpService.deleteTarget(this.short.id).subscribe(data => console.log(data));
        this.short.id = undefined;
      }
      if(this.statusShortTarget != false) this.setAndSendChanges(false);
    }
  }

  setAndSendChanges (isFilled: boolean) {
    this.statusShortTarget = isFilled;
    this.changeStatusShortTarget.emit(isFilled); 
  }

  saveTarget() {
    if(this.short.target_name.length > 1)
      this.httpService.saveTarget(this.short).subscribe(
          data => {
            if (data) {
              this.targetId = data.id;
              this.short.id = this.targetId;
            }
          }
        );
  }

}
