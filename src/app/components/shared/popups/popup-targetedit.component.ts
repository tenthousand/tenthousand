import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { HttpService } from "../../../services/http.service";

import { DateObject } from '../../../models/target';
import { Target } from '../../../models/target';

const monthes = [
  {name: 'Январь', id: 0},
  {name: 'Февраль', id: 1},
  {name: 'Март', id: 2},
  {name: 'Апрель', id: 3},
  {name: 'Май', id: 4},
  {name: 'Июнь', id: 5},
  {name: 'Июль', id: 6},
  {name: 'Август', id: 7},
  {name: 'Сентябрь', id: 8},
  {name: 'Октябрь', id: 9},
  {name: 'Ноябрь', id: 10},
  {name: 'Декабрь', id: 11}
]

@Component({
  selector: 'tt-popup-targetedit',
  template: `
  
    <button *ngIf="previousTarget" (click)="onBackToPreviousTarget()"><-</button>
    <p class="tt-popup__targetedit-title" *ngIf="!editTitle" (click)="onEditTitle()">{{target.target_name}}</p>
    <input class="tt-popup__targetedit-title-edit" (change)="onSaveTarget()" (blur)="editTitle = false" id="popup-edit-title" *ngIf="editTitle" type="text" [(ngModel)]="target.target_name" />
    <div (click)="onEditDescription()" [ngClass]="{'tt-popup__targetedit-title-empty':!target.description}" *ngIf="!editDescription" class="tt-popup__targetedit-description">{{target.description || 'Добавьте обоснование цели'}}</div>
    <textarea *ngIf="editDescription" (change)="onSaveTarget()" (blur)="editDescription = false" [(ngModel)]="target.description" cols="30" rows="10" class="tt-popup__targetedit-description-edit"></textarea>
    <div class="layout-row layout-align-space-between-start">
      <select value="years[0]" class="flex-30 tt-popup__targetedit-deadline" [(ngModel)]="deadline.year" (change)="onDateChanged()">
        <option [value]="year" *ngFor="let year of years">{{year}}</option>
      </select>
      <select value="monthes[0]" class="flex-30 tt-popup__targetedit-deadline" [(ngModel)]="deadline.month" (change)="onDateChanged()" [disabled]="!deadline.year">
        <option [value]="month.id" *ngFor="let month of monthes">{{month.name}}</option>
      </select>
      <select class="flex-30 tt-popup__targetedit-deadline" [ngModel]="deadline.date" (change)="onDeadlineChanged()" [disabled]="!deadline.month || !this.dates.length">
        <option [value]="date" *ngFor="let date of dates">{{date}}</option>
      </select>
    </div>
    <section *ngIf="target.target_type_ref != 3" class="tt-popup__targetedit-children">
      <p class="tt-popup__targetedit-children_title">Дочерние цели:</p>
      <div class="tt-popup__targetedit-child" *ngFor="let child of target.children">
        <span (click)="onChildTargetDelected(child)" class="tt-popup__targetedit-child_name">{{child.target_name}}</span>
      </div>
      <span *ngIf="!newTargetSetting" (click)="onNewTargetAdding()" class="tt-popup__targetedit-newtarget_span">Новая цель...</span>
      <input *ngIf="newTargetSetting" id="popup-edit-new-target" type="text" (blur)="newTargetSetting = false" (change)="onNewTargetAdded($event)" class="tt-popup__targetedit-newtarget_input" />
    </section>
  `,
  styles: []
})
export class PopupTargeteditComponent implements OnInit {

  @Input() target: Target;
  
  previousTarget: Target;
  deadline: DateObject = { year: null, month: null, date: null, };
  years: number[] = [];
  monthes: Array<Object> = monthes;
  dates: number[] = [];
  editTitle: boolean = false;
  editDescription: boolean = false;
  newTargetSetting: boolean = false;

  constructor(private httpService: HttpService) {
    let firstYear = new Date().getFullYear();
    while (this.years.length < 50) {
      this.years.push(firstYear);
      firstYear++;
    }
  }

  onNewTargetAdded(event) {
    let newTarget: Target = { target_name: event.target.value, parent_ref: this.target.id, target_type_ref: this.target.target_type_ref + 1 }
    this.httpService.saveTarget(newTarget).subscribe( data => {
      newTarget.id = data.id;
      this.target.children.push(newTarget);
     });
  }

  ngOnInit() {
    this.getTargetInfo();
  }

  onChildTargetDelected(child) {
    this.previousTarget = this.target;
    this.target = child;
    this.getTargetInfo();
  }

  getTargetInfo() {
    this.httpService.getTargetChildren(this.target.id).subscribe( data => this.target.children = data )
  }

  onBackToPreviousTarget() {
    if (this.previousTarget) {
      this.target = this.previousTarget;
      this.previousTarget = undefined;
    }
  }

  onDateChanged() {
    if (this.deadline.year && this.deadline.month) {
      this.dates = [];
      let daysInMonth = 32 - new Date(this.deadline.year, this.deadline.month, 32).getDate();
      while(daysInMonth > 0){
        this.dates.unshift(daysInMonth);
        daysInMonth--;
      }
    }
  }

  onDeadlineChanged(){
    this.target.deadline = new Date(this.deadline.year, this.deadline.month, this.deadline.date);
    this.onSaveTarget();
  }

  onNewTargetAdding() {
    this.newTargetSetting = true;
    setTimeout(() => document.getElementById('popup-edit-new-target').focus());
  }
  onEditTitle() {
    this.editTitle = true;
    setTimeout(() => document.getElementById('popup-edit-title').focus());
  }
  onEditDescription() {
    this.editDescription = true;
    setTimeout(() => document.getElementById('popup-edit-description').focus());
  }

  onSaveTarget() {
    if(this.target.target_name.length > 1)
      this.httpService.saveTarget(this.target).subscribe(
          data => {
            this.editTitle = false;
            this.editDescription= false;
            console.log(data);
          }
        );
  }

}
