import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { SiteTour } from '../../models/site-tour';

@Component({
  selector: 'tt-sitetour',
  template: `
    <section class="sitetour layout-column layout-align-start-center">
      <div class="sitetour__header w100">
        <h4 class="sitetour__title">{{siteTour.title}}</h4>
      </div>
      <i class="fa fa-times sitetour__close" (click)="closeSiteTour()"></i>
      <p class="sitetour__description">{{siteTour.description}}</p>
      <div class="btn btn-primary mv-8" (click)="closeSiteTour()">Окей</div>
    </section>
  `
})
// <div class="sitetour__arrow">
export class SitetourComponent implements OnInit {

  @Output() changeShow = new EventEmitter<boolean>()

  @Input() siteTour: SiteTour;

  constructor() { }

  ngOnInit() {
  }

  closeSiteTour() {
    this.changeShow.emit(false);
  }

}
