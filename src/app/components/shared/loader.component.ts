import { Component } from '@angular/core';

@Component({
  selector: 'tt-loader',
  template: `
    <section class="tt-loader-shadow layout-row layout-align-center-center">
      <section class="tt-loader-wrapper">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
      </section>
    </section>
  `
})
// <div class="sitetour__arrow">
export class LoaderComponent {

  constructor() { }

}
