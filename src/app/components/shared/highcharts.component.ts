import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'tt-highcharts',
  template: `
  <div [ngSwitch]="type">
    <chart *ngSwitchCase="'doings'" [options]="columnChart"></chart>
    <chart *ngSwitchCase="'donut'" [options]="donutChart"></chart>
    <chart *ngSwitchDefault [options]="defaultChart"></chart>
  </div>
  `
})
    // 
    
export class HighchartsComponent implements OnInit {

  @Input() options: any;
  @Input() type: string;

  constructor() {}

  ngOnInit() {
    if (this.type == 'doings') {
        this.columnChart.xAxis.categories = this.options.doingsNames;
        this.columnChart.series = [{
          name: 'Идеальная рутина',
          data: this.options.idealDoingsTime
      }, {
          name: 'Реальная рутина',
          data: this.options.realDoingsTime
      }]
    }
  }

  public defaultChart = {
      title : { text : 'simple chart' },
      series: [{
          data: [29.9, 71.5, 106.4, 129.2],
      }]
  };

  public donutChart = {
    chart: {
      type: 'pie',
      options3d: {
        enabled: true,
        alpha: 45
      }
    },
    title: {
      text: 'Contents of Highsoft\'s weekly fruit delivery'
    },
    subtitle: {
      text: '3D donut in Highcharts'
    },
    plotOptions: {
      pie: {
        innerSize: 100,
        depth: 45
      }
    },
    series: [{
      name: 'Delivered amount',
      data: [
        ['Bananas', 8],
        ['Kiwi', 3],
        ['Mixed nuts', 1],
        ['Oranges', 6],
        ['Apples', 8],
        ['Pears', 4],
        ['Clementines', 4],
        ['Reddish (bag)', 1],
        ['Grapes (bunch)', 1]
      ]
    }]
  };

  public columnChart = {
    chart: {
          type: 'column'
      },
      title: {
          text: 'Historic World Population by Region'
      },
      subtitle: {
          text: 'Source: <a href="https://en.wikipedia.org/wiki/World_population">Wikipedia.org</a>'
      },
      xAxis: {
          categories: ['Africa', 'America', 'Asia', 'Europe', 'Oceania'],
          title: {
              text: null
          }
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Population (millions)',
              align: 'high'
          },
          labels: {
              overflow: 'justify'
          }
      },
      tooltip: {
          valueSuffix: ' минут'
      },
      plotOptions: {
          bar: {
              dataLabels: {
                  enabled: true
              }
          }
      },
      legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'top',
          x: -40,
          y: 80,
          floating: true,
          borderWidth: 1,
          backgroundColor: '#FFFFFF',
          shadow: true
      },
      credits: {
          enabled: false
      },
      series: [{
          name: 'Идеальная рутина',
          data: [107, 31, 635, 203, 2]
      }, {
          name: 'Реальная рутина',
          data: [133, 156, 947, 408, 6]
      }]
  };
};
