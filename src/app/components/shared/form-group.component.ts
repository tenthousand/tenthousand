import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '../../models/form-group';


@Component({
  selector: 'tt-form-group',
  template: `
    <input [(ngModel)]="bindmodel.value" [ngClass]="{'form-control-input-error': !visibleValidate}" (blur)="onInputBlur()" (keyup)="validateValue($event)" required type="{{bindmodel.type}}" class="form-control-input" id="{{bindmodel.id}}">
    <label class="form-control-label" [ngClass]="{'form-control-label-up': labelUp, 'form-control-label-error': !visibleValidate}" for="{{bindmodel.id}}">{{ visibleValidate ? bindmodel.name : bindmodel.error }}</label>
  `
})
export class FormGroupComponent {

  public visibleValidate: boolean = true;
  public labelUp: boolean;
  public inputIsTouched: boolean = false;

  @Output() validChange = new EventEmitter()
  @Input() bindmodel: FormGroup;

  constructor () {}

  validateValue () {

    if (this.bindmodel.value.length) this.labelUp = true;
    else {
      this.labelUp = false;
    }
    let validate;
    if (this.bindmodel.type == 'email') {
      if (/.+@.+\..+/.test(this.bindmodel.value)) validate = true;
      else validate = false;
    } else if (this.bindmodel.type == 'password') {
      if (this.bindmodel.value.length >= 6) validate = true;
      else validate = false;
    }

    this.validChange.emit(validate);

    if (this.inputIsTouched && validate != this.visibleValidate) {
      this.visibleValidate = validate;
    }
  }

  onInputBlur () {
    this.inputIsTouched = true;
    this.validateValue();
  }

}
