import { Component } from '@angular/core';
import { Response } from '@angular/http';
import { Router, ActivatedRoute } from "@angular/router";

import { Observable } from "rxjs/Rx";

import { User } from '../../models/user';
import { FormGroup } from '../../models/form-group';

import { HttpService } from "../../services/http.service";
import { AccountService } from '../../services/account.service';

@Component({
  selector: 'tt-account-action',
  template: `
    <div class="w100 pv-8 layout-column layout-align-start-center tt-account-title">
      <h1>10000 часов</h1>
    </div>
    <form #account="ngForm" [ngClass]="{'disp-n':accountActionSent}" class="tt-account-action ph-32 pv-16 layout-column layout-align-start-start">
      <tt-form-group [bindmodel]="emailModel" (validChange)="onEmailValidChange($event)"></tt-form-group>
      <tt-form-group [bindmodel]="passwordModel" (validChange)="onPasswordValidChange($event)"></tt-form-group>
      <p class="tt-account-note m-0">* Впервые на сайте? Введите любой пароль и вы зарегистрируетесь!</p>
      <button type="button" [disabled]="!accountActionFormIsValid" class="mv-8 w100 btn btn-primary" (click)="onAccountAction()">Войти</button>      
    </form>
    <div [ngClass]="{'disp-n':!accountActionSent}"><i class="fa fa-spinner fa-spin tt-account-action-loader"></i></div>
  `
})
export class AccountActionComponent {

  items: any[] = [];

  public accountActionSent: boolean = false;
  public emailValid: boolean = false;
  public passwordValid: boolean = false;
  public accountActionFormIsValid: boolean = false;
  public settings: any;

  public emailModel: FormGroup = { id: 'tt-account-action-email', value: '', type: 'email', name: 'Email', error: 'Введите корректный email' }
  public passwordModel: FormGroup = { id: 'tt-account-action-password', value: '', type: 'password', name: 'Пароль', error: 'Не менее 6-ти символов' }

  constructor(private accountService: AccountService, private router: Router, private activatedRoute: ActivatedRoute, private httpService: HttpService) {}

  onEmailValidChange(e) {
    this.emailValid = e;
    this.checkAccountActionFormIsValid();
  }
  onPasswordValidChange(e) {
    this.passwordValid = e;
    this.checkAccountActionFormIsValid();
  }
  checkAccountActionFormIsValid() {
    if (this.emailValid && this.passwordValid) this.accountActionFormIsValid = true;
    else this.accountActionFormIsValid = false;
  }

  onAccountAction() {
    this.httpService.logIn({email:this.emailModel.value, password: this.passwordModel.value})
      .subscribe(
        data => {
          this.accountService.onAuthorized(true);
          localStorage['authKey'] = data.token;
          this.router.navigate(['/targets']);
        }
      );
  }
}