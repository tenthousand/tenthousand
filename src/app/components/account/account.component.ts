import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';

import { LoaderService } from '../../services/loader.service';

@Component({
  selector: 'tt-account',
  template: `
    <div class="tt-account-image layout-column layout-align-center-center">
    </div>
    <section class="tt-account-content layout-column layout-align-center-center">
      <tt-account-action class="layout-column layout-align-center-start"></tt-account-action>
    </section>
  `
})
export class AccountComponent implements OnInit {

  constructor(private loaderService: LoaderService) { }

  ngOnInit() {
    this.loaderService.onSwitchLoader(false);
  }

}
