import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { Task } from '../../models/task';
import { Month } from '../../models/calendar';

@Component({
  selector: 'tt-calendar-day',
  template: `
    <div class="layout-row layout-align-center-center tt-calendar__days-one previous" (click)="goToPeriod(day, currentMonth.monthNumber > 0 ? currentMonth.year : currentMonth.year - 1, currentMonth.monthNumber > 0 ? currentMonth.monthNumber - 1 : 11)" *ngFor="let day of previousMonth.days">
      <div title="Запланировано задач" *ngIf="day.tasks && day.tasks.length" class="tt-calendar__period_tasks">{{day.tasks.length}}</div>
      <div title="Отчет по дню сделан" *ngIf="day.alreadyReported" class="absolute tt-calendar__period_reported"><i class="fa fa-check"></i></div>
      <span class="tt-calendar__days-one_number">{{day.date}}</span>
    </div>
    <div class="layout-row layout-align-center-center tt-calendar__days-one current" (click)="goToPeriod(day, currentMonth.year, currentMonth.monthNumber)" *ngFor="let day of currentMonth.days">
      <div title="Запланировано задач" *ngIf="day.tasks && day.tasks.length" class="tt-calendar__period_tasks">{{day.tasks.length}}</div>
      <div title="Отчет по дню сделан" *ngIf="day.alreadyReported" class="absolute tt-calendar__period_reported"><i class="fa fa-check"></i></div>
      <span class="tt-calendar__days-one_number">{{day.date}}</span>
    </div>
    <div class="layout-row layout-align-center-center tt-calendar__days-one next" (click)="goToPeriod(day, currentMonth.monthNumber < 11 ? currentMonth.year : currentMonth.year + 1, currentMonth.monthNumber < 11 ? currentMonth.monthNumber + 1 : 0)" *ngFor="let day of nextMonth.days">
      <div title="Запланировано задач" *ngIf="day.tasks && day.tasks.length" class="tt-calendar__period_tasks">{{day.tasks.length}}</div>
      <div title="Отчет по дню сделан" *ngIf="day.alreadyReported" class="absolute tt-calendar__period_reported"><i class="fa fa-check"></i></div>
      <span class="tt-calendar__days-one_number">{{day.date}}
    </span></div>
  `,
  styles: []
})
export class CalendarDayComponent implements OnInit {

  @Input() previousMonth: Month;
  @Input() currentMonth: Month;
  @Input() nextMonth: Month;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToPeriod(day, year, month) {
    let startDate = day.date;
    this.router.navigate(['/period' + (day.alreadyReported ? '/stat' : day.tasks && day.tasks ? '/report' : '/plan')], {queryParams: {year, month, startDate, dateType: 1}});
  }

}
