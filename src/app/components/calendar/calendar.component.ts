import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

import { Task } from '../../models/task';
import { Year, Month, Week, Period } from '../../models/calendar';
import {  } from '../../models/calendar';

import { HttpService } from "../../services/http.service";
import { LoaderService } from '../../services/loader.service';

const monthes: string[] = ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь']

@Component({
  selector: 'tt-calendar',
  template: `
    <div class="tt-calendar layout-row">
      <section class="layout-column layout-align-space-around-center flex-5 tt-calendar__year">
        <span class="mv-16 tt-calendar__arrows" (click)="setCalendar('year',-1)"><i class="fa fa-arrow-up"></i></span>
        <span class="tt-calendar__year_title rotate_270" (click)="goToPeriod(4, currentYear, currentMonth.year)">
          <span>{{currentMonth.year}}</span>
          <span style="margin: 9px;" title="Запланировано задач" *ngIf="currentYear.tasks && currentYear.tasks.length" class="tt-calendar__period_tasks">{{currentYear.tasks.length}}</span>
          <span title="Отчет по году сделан" *ngIf="currentYear.alreadyReported" class="vertical tt-calendar__period_reported"><i class="fa fa-check"></i></span>
        </span>
        <span class="mv-16 tt-calendar__arrows" (click)="setCalendar('year',1)"><i class="fa fa-arrow-down"></i></span>
      </section>
      <section class="layout-column layout-align-space-around-center flex-5 tt-calendar__month">
        <span class="mv-16 tt-calendar__arrows" (click)="setCalendar('month',-1)"><i class="fa fa-arrow-up"></i></span>
        <span class="tt-calendar__month_title rotate_270" (click)="goToPeriod(3, currentMonth, currentMonth.year, currentMonth.monthNumber)">
          <span>{{monthName}}</span>
          <span style="margin: 9px;" title="Запланировано задач" *ngIf="currentMonth.tasks && currentMonth.tasks.length" class="tt-calendar__period_tasks">{{currentMonth.tasks.length}}</span>
          <span title="Отчет по месяцу сделан" *ngIf="currentMonth.alreadyReported" class="vertical tt-calendar__period_reported"><i class="fa fa-check"></i></span>
        </span>
        <span class="mv-16 tt-calendar__arrows" (click)="setCalendar('month',1)"><i class="fa fa-arrow-down"></i></span>
      </section>
      <tt-calendar-day class="flex-75 tt-calendar__days layout-row layout-align-space-between-center layout-wrap" [previousMonth]="previousMonth" [currentMonth]="currentMonth" [nextMonth]="nextMonth"></tt-calendar-day>
      <section class="flex-15 tt-calendar__weeks layout-column">
        <div class="layout-row layout-align-center-center tt-calendar__weeks-one" (click)="goToPeriod(2, week, week.firstDay.year, week.firstDay.month, week.firstDay.date, week.lastDay.date, week.lastDay.month, week.lastDay.year)" *ngFor="let week of weeksInMonth; let weekIndex = index">
          <div title="Запланировано задач" *ngIf="week.tasks && week.tasks.length" class="tt-calendar__period_tasks">{{week.tasks.length}}</div>
          <div title="Отчет по неделе сделан" *ngIf="week.alreadyReported" class="absolute tt-calendar__period_reported"><i class="fa fa-check"></i></div>
          <span class="tt-calendar__weeks-one_number">{{weekIndex + 1}}-я неделя</span>
        </div>
      </section>
    </div>
  `,
  styles: []
})
export class CalendarComponent implements OnInit {
  
  monthName: string;
  // countWeek_week: number;
  tasksTypesCount: number;
  weeksInMonth: Week[];
  currentYear: Year;
  previousMonth: Month;
  currentMonth: Month;
  nextMonth: Month;
  tasks: Task[];

  constructor(private router: Router, private httpService: HttpService, private loaderService: LoaderService) { }

  getMonthInfo(year, month) {
    let currentMonthDate = new Date(year, month,1);
    let monthNumber = currentMonthDate.getMonth();
    let monthYear = currentMonthDate.getFullYear()
    let daysInMonth = 32 - new Date(monthYear, monthNumber, 32).getDate();
    let firstDay = currentMonthDate.getDay();
    let d = 0;
    let days = [];
    while(d < daysInMonth){ days.push({date:d+1}); d++; }
    return { year: monthYear, monthNumber, daysInMonth, firstDay, days }
  }

  ngOnInit() {
    this.setCalendar();
    this.tasksTypesCount = 0;
  }

  goToPeriod(dateType, period, year, month, startDate, endDate, endMonth, endYear) {
    let tool = period.alreadyReported ? '/stat' : period.tasks && period.tasks.length ? '/report' : '/plan';
    if (month !== undefined && startDate !== undefined && endDate !== undefined) this.router.navigate(['/period'+tool], {queryParams: {year, month, startDate, endDate, endMonth, endYear, dateType}});
    else if (month !== undefined && startDate !== undefined) this.router.navigate(['/period'+tool], {queryParams: {year, month, startDate, dateType}});
    else if (month !== undefined) this.router.navigate(['/period'+tool], {queryParams: {year, month, dateType}});
    else if (year) this.router.navigate(['/period'+tool], {queryParams: {year, dateType}});
  }

  setCalendar(changeType?, changeDirection?) {
    let currentDate;
    if (changeType && this.currentMonth){
      currentDate = new Date(
        changeType == 'year' ? this.currentMonth.year + changeDirection : this.currentMonth.year,
        changeType == 'month' ? this.currentMonth.monthNumber + changeDirection : this.currentMonth.monthNumber
      );
    } else
       currentDate = new Date();
    let year = currentDate.getFullYear(), 
        month = currentDate.getMonth();
    this.currentYear = {tasks:[]}
    this.previousMonth = this.getMonthInfo(year, month-1);
    this.currentMonth = this.getMonthInfo(year, month);
    this.nextMonth = this.getMonthInfo(year, month+1);

    var previousSplicedDays = this.currentMonth.firstDay === 0 ? 6 : this.currentMonth.firstDay - 1;
    var nextSplicedDays = new Date(year, month, this.currentMonth.daysInMonth-1).getDay();

    this.previousMonth.days = this.previousMonth.days.splice(-previousSplicedDays, previousSplicedDays);
    this.nextMonth.days = this.nextMonth.days.splice(0, 6 - nextSplicedDays);
  
    if (this.previousMonth.days.length + this.currentMonth.days.length + this.nextMonth.days.length < 42) {
      let nextLength = this.nextMonth.days.length;
      let i = 1;
      while (i < 8) { this.nextMonth.days.push({date: nextLength + i}); i++; }
    }

  /* Попытка присваивать номер недели */
    // let countWeek_month = new Date(year, month, this.currentMonth.days[0].date).getMonth();
    // let countWeek_firstDayInYear = new Date(year,0,1).getDay();
    // let countWeek_startMonth = 0;
    // let countWeek_days = 0;
    // while(countWeek_startMonth < countWeek_month){
    //   countWeek_days += 32 - new Date(year, countWeek_startMonth, 32).getDate();
    //   countWeek_startMonth++;
    // }
    // this.countWeek_week = Math.floor(countWeek_days/7);
    
    // let  l=new Date(year, 1, 0);
    // let weeks = (Math.ceil( (l.getDate()- (l.getDay()?l.getDay():7))/7 )+1);
    // if (weeks == 6 && countWeek_month !== 0)
    //   this.countWeek_week++;

    this.weeksInMonth = [];
    if (this.previousMonth.days.length)
      this.weeksInMonth.push({
        firstDay: {date: this.previousMonth.days[0].date,month: this.previousMonth.monthNumber,year: this.previousMonth.year},
        lastDay: {date: this.currentMonth.days[6 - this.previousMonth.days.length].date,month: this.currentMonth.monthNumber,year: this.currentMonth.year}
      })
    let currentMonthDate = this.previousMonth.days.length ? 7 - this.previousMonth.days.length : 0;
    while(currentMonthDate < this.currentMonth.days.length) {
      this.weeksInMonth.push({
        firstDay: {date: this.currentMonth.days[currentMonthDate].date,month: this.currentMonth.monthNumber,year: this.currentMonth.year},
        lastDay: this.currentMonth.days[currentMonthDate+6] 
         ? 
        {date: this.currentMonth.days[currentMonthDate+6].date,month: this.currentMonth.monthNumber,year: this.currentMonth.year}
         : 
        {date: this.nextMonth.days[5 - nextSplicedDays].date,month: this.nextMonth.monthNumber,year: this.nextMonth.year}

      });
      currentMonthDate += 7;
    }
    if (this.weeksInMonth.length < 6)
      this.weeksInMonth.push({
        firstDay:{date:this.nextMonth.days.length-6, month: this.nextMonth.monthNumber,year: this.nextMonth.year},
        lastDay:{date:this.nextMonth.days.length, month: this.nextMonth.monthNumber,year: this.nextMonth.year}
      })
    this.monthName = monthes[this.currentMonth.monthNumber];

    let startDate = this.previousMonth.days.length ? '' + this.previousMonth.year + this.setDateNumber(this.previousMonth.monthNumber+1) + this.setDateNumber(this.previousMonth.days[0].date) : '' + this.currentMonth.year + this.setDateNumber(this.currentMonth.monthNumber+1) + this.setDateNumber(this.currentMonth.days[0].date);
    let endDate = this.nextMonth.days.length ? '' + this.nextMonth.year + this.setDateNumber(this.nextMonth.monthNumber+1) + this.setDateNumber(this.nextMonth.days[this.nextMonth.days.length - 1].date) : '' + this.currentMonth.year + this.setDateNumber(this.currentMonth.monthNumber+1) + this.setDateNumber(this.currentMonth.days[this.currentMonth.days.length -1].date)

    let daysPeriod = {dateType: 1, startDate, endDate};
    let weeksPeriod = {dateType: 2, startDate, endDate};
    let monthPeriod = {dateType: 3, startDate: '' + this.currentMonth.year + this.setDateNumber(this.currentMonth.monthNumber+1) + '01', endDate: '' + (this.currentMonth.monthNumber == 11 ? this.nextMonth.year : this.currentMonth.year) + this.setDateNumber(this.nextMonth.monthNumber+1) + '01'}; 
    let yearPeriod = {dateType: 4, startDate: '' + this.currentMonth.year + '01' + '01', endDate: '' + (this.currentMonth.year + 1) + '01' + '01'};

    this.httpService.getTasksList(daysPeriod).subscribe(
      data => {
        this.tasks = data.tasks;
        this.tasks.map(task => {
          let date = task.date.toString();
          task.date = {year: parseInt(date.slice(0,4)), month: parseInt(date.slice(4,6))-1, day: parseInt(date.slice(6,8))}
          return task;
        });
        this.mapTasksToDays();
        this.tasksTypesCount++;
        checkTasksCount();
      }
    );
    this.httpService.getTasksList(weeksPeriod).subscribe(data => {
        let weeksTasks = data.tasks;
        weeksTasks.map(task => {
          let date = task.date.toString();
          task.date = {year: parseInt(date.slice(0,4)), month: parseInt(date.slice(4,6))-1, day: parseInt(date.slice(6,8))}
          return task;
        });
        weeksTasks.forEach(task => this.weeksInMonth.forEach(week => {
            if (task.date.day == week.lastDay.date && task.date.month == week.lastDay.month) {
              if (!week.tasks) week.tasks = [];
              week.tasks.push(task);
              if (task.isDone && !week.alreadyReported) week.alreadyReported = true;
            }
        }));
        this.tasksTypesCount++;
        checkTasksCount();
    });
    this.httpService.getTasksList(monthPeriod).subscribe(data => {
      this.currentMonth.tasks = data.tasks;
      this.currentMonth.tasks.forEach(task => {if (task.isDone && !this.currentMonth.alreadyReported) this.currentMonth.alreadyReported = true; })
      this.tasksTypesCount++;
      checkTasksCount();
    });
    this.httpService.getTasksList(yearPeriod).subscribe(data => {
      this.currentYear.tasks = data.tasks;
      this.tasksTypesCount++;
      this.currentYear.tasks.forEach(task => {if (task.isDone && !this.currentYear.alreadyReported) this.currentYear.alreadyReported = true; })
      checkTasksCount();
    });

    let that = this;
    let checkTasksCount = function () {
      if (that.tasksTypesCount == 4)
        that.loaderService.onSwitchLoader(false);
    }

  }

  mapTasksToDays () {
    this.tasks.forEach(task => {
      if (task.date.month == this.previousMonth.monthNumber) pushTaskToDay(this.previousMonth, task);
      else if (task.date.month == this.currentMonth.monthNumber) pushTaskToDay(this.currentMonth, task);
      else if (task.date.month == this.nextMonth.monthNumber) pushTaskToDay(this.nextMonth, task);
    });

    function pushTaskToDay (month: Month, task: Task) {
      month.days.forEach(day => {
        if (task.date.day == day.date) {
          if (!day.tasks) day.tasks = [];
          day.tasks.push(task);
          if (task.isDone && !day.alreadyReported) day.alreadyReported = true;
        }
      })
    }
    console.log(this.currentMonth);
  }

  setDateNumber (date) {
    return date > 9 ? '' + date : '0' + date;
  }

}

/* 
*/