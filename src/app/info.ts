// this.router.navigate(['/'], {queryParams: {'analytics':100}}); // Переход с ?analytics=100
// <a [routerLink]="['/account', id.value]">Login</a> // Переход по html-ссылке на account/123
// <input type="text" #id (input)="0">

// ----------- //

// Асинхронное отображение полученных с сервера данных //

// <p>{{asyncString | async}}</p>
// asyncString: Observable<any> = this.httpService.getData('httpLearninig');

// ----------- //

// Получение и отображение данных //

//   <ul>
//     <li *ngFor="let item of items">{{item.email}}</li>
//   </ul>

//   onGetData () {
//     this.httpService.getOwnData()
//       .subscribe(
//         data => {
//           const myArray = [];
//           for (let key in data) {
//             myArray.push(data[key]);
//           }
//           this.items = myArray;
//         }
//       )
//   }


// app.zIndex = {
//     loader: 110
//     sitetour: 100
//     popup: 10
//     targets_ref: 5
// }
