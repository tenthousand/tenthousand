import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core"

import { MainComponent } from "./components/main/main.component";
import { AccountComponent } from "./components/account/account.component";
import { HighchartsComponent } from "./components/shared/highcharts.component";
import { CalendarComponent } from "./components/calendar/calendar.component";
import { PeriodComponent } from "./components/period/period.component";
import { RoutineComponent } from "./components/routine/routine.component";
import { TargetsComponent } from "./components/targets/targets.component";
import { TargetsSettingsComponent } from "./components/targets/targets-settings.component";
import { TargetsOverviewComponent } from "./components/targets/targets-overview.component";

import { TARGETS_ROUTES } from './components/targets/targets.routes'
import { ROUTINE_ROUTES } from './components/routine/routine.routes'
import { PERIOD_ROUTES } from './components/period/period.routes'

const APP_ROUTES: Routes = [
    { path: 'account', component: AccountComponent },
    { path: 'highcharts', component: HighchartsComponent },
    { path: 'calendar', component: CalendarComponent },
    { path: 'period', component: PeriodComponent },
    { path: 'period', component: PeriodComponent, children: PERIOD_ROUTES },
    { path: 'routine', component: RoutineComponent },
    { path: 'routine', component: RoutineComponent, children: ROUTINE_ROUTES },
    { path: 'targets', component: TargetsComponent },
    { path: 'targets', component: TargetsComponent, children: TARGETS_ROUTES },
    { path: '', component: MainComponent }
]

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);