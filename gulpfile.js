var gulp = require('gulp'),
    watch = require('gulp-watch'),
    sass = require('gulp-sass')
    
var path = {
    build: { //Тут мы укажем куда складывать готовые после сборки файлы
        css: 'src/'
    },
    src: { //Пути откуда брать исходники
        style: 'src/assets/styles/styles.scss'
    },
    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
        style: 'src/assets/styles/**/*.scss'
    },
};

gulp.task('style:build', function () {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(sass()) //Скомпилируем
        .pipe(gulp.dest(path.build.css)) //И в build
        //.pipe(reload({stream: true}));
});

gulp.task('watch', function(){
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
});